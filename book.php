<?php include_once 'dbconfig.php';
    $book=$_GET['book'];
	$q="SELECT * FROM posts where link = '$book'";
	$result=$conn->query($q);
	if($result->num_rows > 0){
	    while($row = $result->fetch_assoc()){
            $post_id=$row['id'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="uHub">
    <meta name="robots" content="index,follow,noodp">
    <meta name="googlebot" content="index,follow">
    <meta property="og:locale" content="fa_IR">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="uHub">
    <meta name="theme-color" content="dark">
    <title>UHub Books - <?php echo $row['title']; ?></title>
    <link type="text/css" rel="stylesheet" href="static/plugins/materialize/css/materialize.min.css"/>
    <link href="static/css/materialIcons.css" rel="stylesheet">
    <link href="static/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
    <link href="static/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="static/plugins/slick/slick.css">
    <link rel="stylesheet" href="static/plugins/slick/slick-theme.css">
    <!-- Custom CSS -->
    <link href="static/css/uhub-custom.css" rel="stylesheet" type="text/css"/>
    <!--Theme-->
    <link href="static/themes/dark_pink.css" rel="stylesheet" type="text/css"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="dark-custom">
<div class="loader-bg dark-custom"></div>
<div class="loader">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
        <div class="spinner-layer spinner-teal lighten-1">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
        <div class="spinner-layer spinner-yellow">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
        <div class="spinner-layer spinner-green">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
</div>
<div class="mn-content fixed-sidebar">
    <header class="mn-header navbar-fixed">
        <nav class="dark-custom">
            <div class="nav-wrapper row ">
                <div class="header-title col s3 hide-on-small-and-down">
                    <span style="font-weight: bolder" class="chapter-title"><b>uHub</b></span>
                </div>
                <div class="header-title col s1 push-s5 hide-on-med-and-up center">
                    <span style="font-weight: bolder" class="chapter-title"><b>uHub</b></span>
                </div>
                <form class="left search col s6 hide-on-small-and-down">
                    <div class="input-field">
                        <input id="search" style="font-size: 200px;line-height: 3;" class="right-align right" dir="auto"
                               type="search" placeholder="جستجو" autocomplete="off">
                    </div>
                    <a class="close-search waves-effect waves-dark"><i class="material-icons">close</i></a>
                </form>
                <ul class="right col s6 m3 nav-right-menu">
                    <li><a data-activates="slide-out" data-activates-lol="chat-sidebar"
                           class="chat-button show-on-large waves-effect waves-light">
                        <i class="material-icons" id="slide-out-icon">menu</i></a>
                    </li>
                    <li class="hide-on-med-and-up">
                        <a class="search-toggle waves-effect waves-light">
                            <i class="material-icons">search</i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside id="slide-out" class="side-nav dark-custom">
        <div class="side-nav-wrapper">
            <div if="{user}" class="sidebar-profile">
                <div class="sidebar-profile-image right-align">
                    <img src="{user.avatar? user.avatar : '/static/assets/images/profile-image-2.png'}" class="circle"
                         alt="">
                </div>
                <div class="sidebar-profile-info">
                    <a href="javascript:void(0);" class="account-settings-link">
                        <p style="" dir="auto" class="right right-align">{user.first_name} {user.last_name}</p>
                        <span class="white-text-custom">@{user.username}</span>
                    </a>
                </div>
            </div>
            <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
                <li onclick="close_sidenav()" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-60px;font-size:110%"><b class="">صفحه اصلی</b></span><i
                        class="material-icons right">home</i></a></li>
                <li onclick="close_sidenav()" if="{!user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b class="">ثبت نام</b></span><i
                        class="material-icons right">person_add</i></a></li>
                <li onclick="close_sidenav()" if="{!user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b class="">ورود</b></span><i
                        class="material-icons right">input</i></a></li>
                <li onclick="close_sidenav()" if="{user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b class="">پروفایل</b></span><i
                        class="material-icons right">account_circle</i></a></li>
                <li onclick="close_sidenav()" if="{user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b class="">تنظیمات</b></span><i
                        class="material-icons right">settings</i></a></li>
                <li onclick="close_sidenav()" if="{user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b style="padding-left: 10px"
                                                                     class="">گالری</b></span><i
                        class="material-icons right">collections</i></a></li>
                <li onclick="close_sidenav()" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-30px;font-size:110%"><b style="padding-left: 10px"
                                                                     class="">عکس های جدید</b></span><i
                        class="material-icons right">fiber_new</i></a></li>
                <li onclick="close_sidenav()" class="no-padding center {opts.items.theme.class_2}"><a
                        onclick="Materialize.toast('! به زودی', 4000)" class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b style="padding-right: 15px" class="">مرکز آموزش</b></span><i
                        class="material-icons right">local_library</i></a></li>

            </ul>
            <div class="footer">
                <p class="copyright">Bamboo ©</p>
            </div>
        </div>
    </aside>
    <nav class="nav-extended" id="ctcontainer">
        <div class="row">
            <div class="col s5 m4 l2">
                <div class="uhub-image">
                    <img src="img/<?php echo $row['image'] ?>" alt="<?php echo $row['title'] ?>" id="image" class="responsive-img">
                </div>
            </div>
            <div class="col s7 m8 l10">
                <h5 class="title"><?php echo $row['title'] ?></h5>
                <h6><?php echo $row['author'] ?></h6>
                <?php
                    $q3="SELECT * FROM `tags` where post_id='$post_id'";
                    $result3=$conn->query($q3);
                    if($result3->num_rows > 0){
                        while($row3 = $result3->fetch_assoc()){ 
                            $tag=$row3['tag']; ?>
                            <a href="tag.php?tag=<?php echo $tag ?>"><div class="chip"><?php echo $tag ?></div></a>
                <?php } } ?>
                <br>
                <div class="btn dark-pink waves-effect waves-light"><a href="ups/<?php echo $row['file'] ?>">Download</a></div>
            </div>
        </div>
    </nav>
    <div class="container white-text">
        <p><?php echo $row['content']; } }?></p>
    </div>

    <hr>
    <h4>Related</h4>
    <div class="container white-text">
	<?php
	    $q4="SELECT * FROM `tags` where tag = '$tag' ORDER BY RAND() LIMIT 4";
	    $result4=$conn->query($q4);
	    if($result4->num_rows > 0){
	        while($row4 = $result4->fetch_assoc()){
                $posts=$row4['post_id'];
                $q5="SELECT * FROM `posts` where id='$posts'";
                $result5=$conn->query($q5);
                if($result5->num_rows > 0){
                    while($row5 = $result5->fetch_assoc()){ ?>
                        <a href="<?php echo $row5['post_type'] ?>.php?<?php echo $row5['post_type'] ?>=<?php echo $row5['link'] ?>"><img width="100" height="100" src="img/<?php echo $row5['image'] ?>"></a><?php echo $row5['title']; ?><br>
                <?php }
                }
            } 
        } ?>
    </div>

    <hr>
    <h4>Reviews</h4>
	<div class="row">
        <form id="review" action="review.php" method="POST" class="col l6 s12">
           <div class="stars">
	            <input class="Small material-icons" id="star-5" type="radio" value="5" name="rating"/>
                <label class="Small material-icons" for="star-5">grade</label>
                <input class="Small material-icons" id="star-4" type="radio" value="4" name="rating"/>
                <label class="Small material-icons" for="star-4">grade</label>
                <input class="Small material-icons" id="star-3" type="radio" value="3" name="rating"/>
                <label class="Small material-icons" for="star-3">grade</label>
                <input class="Small material-icons" id="star-2" type="radio" value="2" name="rating"/>
                <label class="Small material-icons" for="star-2">grade</label>
                <input class="Small material-icons" id="star-1" type="radio" value="1" name="rating"/>
                <label class="Small material-icons" for="star-1">grade</label>
            </div>
            <div class="row">
                <div class="input-field col l3 s12">
                    <input id="name" name="name" type="text" class="validate">
                    <label for="name">Name</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col l3 s12">
	                <textarea id="review" name="review" class="materialize-textarea"></textarea>
                    <label for="review">Review</label>
                </div>
            </div>
	        <button class="btn waves-effect waves-light" type="submit" name="submit">Submit
	            <i class="material-icons right">send</i>
	        </button>
            <input id="id" name="post_id" type="hidden" value="<?php echo $post_id; ?>">
        </form>
	</div>
    
	<?php
	    $q2="SELECT * FROM reviews where post_id = '$post_id;'";
	    $result2=$conn->query($q2);
	    if($result2->num_rows > 0){
	        while($row2 = $result2->fetch_assoc()){ ?>
	            <div class="container white-text">
	                <?php 
                        $stars=$row2['rating'];
                        for($i=0;$i<$stars;$i++){
                            echo '<i class="Small material-icons">grade</i>';
                        }
	                    echo "<br>Name: ".$row2['name']."<br>";
                        echo "Comment: ".$row2['review']."<br>";
                    ?>
                </div>
	<?php } }
        else{ ?>
	        <div class="container white-text">
                <?php echo 'No reviews available. Be first to comment!'; ?>
            </div>
     <?php
        }
     ?>

</div>
<!--Import Scripts-->
<script src="static/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="static/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="static/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="static/plugins/slick/slick.min.js"></script>
<script src="static/plugins/color-thief/color-thief.js"></script>
<script src="static/plugins/sweetalert/sweetalert.min.js"></script>
<script src="static/plugins/materialize/js/materialize.min.js"></script>
<script src="static/js/alpha.js"></script>
<script src="static/js/uhub.js"></script>
</body>
</html>