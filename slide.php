<?php
date_default_timezone_set('Asia/Tehran');
include_once 'dbconfig.php';
$caption = @$_POST['caption'];
$description = @$_POST['description'];
$link = @$_POST['link'];
	if(isset($_POST['post'])) {
		$timestamp = date('Y-m-d G:i:s');
		$uploaded = rand(1000,100000)."-slide-".$_FILES['image']['name'];
		$file_loc = $_FILES['image']['tmp_name'];
		$new_file_name = strtolower($uploaded);
		$imgs="img/";
		$final_file = str_replace(' ','-',$new_file_name);
		move_uploaded_file($file_loc,$imgs.$final_file);

		$sql="INSERT INTO slides(image,caption,description,link,post_time) VALUES('$final_file','$caption','$description','$link','$timestamp')";
		
		if ($conn->query($sql) === TRUE) {
			echo "submited!";
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}
		$conn->close();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>UHub - New Slide</title>
	<script src="js/jquery-3.1.1.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="/uhub/ckeditor/ckeditor.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<style>
		body{
			background-color: lightslategray;
		}
		a{
			color: white;
			transition: all 0.35s ease-out;
		}
		a:hover{
			color: black;
			text-decoration: none;
		}
		a:visited{
			text-decoration: none;
		}
		a:link{
			color: white;
			text-decoration: none;
		}
		.row{
			margin: 30px auto;
			text-align: center;
		}
		.head{
			background-color: darkorange;
			height: 50px;
			color: white;
			text-align: center;
			font-size: 35px;
		}
		form{
			border: 2px solid darkorange;
			padding: 10px;
			border-radius: 10px;
			background-color: rgba(249, 168, 69, 0.21);
			box-shadow: 3px 6px 17px 1px #777777;
		}
		h4{
			color: white;
			background-color: darkorange;
			border-radius: 15px;
			padding: 5px;
		}
		input[type=text]{
			display: inline-block;
			width: 100%;
			margin-bottom: 15px; 
		}
		input[type=file]{
			display: inline-block;
			margin: 0 0 15px 10px; 
		}
		textarea{
			width: 100%;
		}
		input[type=submit]{
			display: inline-block;
			background-color: darkorange;
			color: white;
			border: 0px;
			transition: all 0.3s linear;
			width: 100%;
			margin: 5px 0 15px 0;
			box-shadow: 3px 6px 17px 1px #777777;
 		}
		input[type=submit]:hover{
			background-color: #d47706;
			box-shadow: 0px 0px 07px 0px #777777;
		}
		input[type=radio]{
			margin: 5px;
		}
		label{
			color: white;
		}
		.footer{
			visibility: visible;
			background-color: darkorange;
			height: 50px;
			padding: 15px;
			color: white;
			bottom: 0px;
			left: 0px;
			position: fixed;
			text-align: center;
			width: 100%;
			z-index: 99;
			box-shadow: 0px 0px 07px 0px #777777;
		}
		.separator{
			padding: 0 15px;
		}
	</style>
</head>

<body>
	<div id="top" class="head">New Slide</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-xs-12 col-md-offset-3">
				<form action="slide.php" method="POST" enctype="multipart/form-data" required>
					<label>Slide Image:</label><input type="file" name="image" id="cover" accept="image/*"  required/>
					<img id="preview" src="img/no-image-slide.png" width="100" height="100">
					<input type="text" placeholder="Caption" name="caption">
					<input type="text" placeholder="Link" name="link">
					<textarea rows="8" placeholder="Slide Description" name="description"></textarea>
					<input type="submit" value="Post" name="post">
				</form>
			</div>
		</div>
		<div style="margin-top: 100px"></div>
		<div id="footer" class="footer"><a href="./index.php"><i class="fa fa-home fa-lg"> Homepage</i></a><label class="separator"> | </label><a href="http://localhost/uhub/logout.php"><i class="fa fa-user fa-lg"> Logout</i></a></div>
	</div>
	<script src="static/js/uhub.js"></script>
</body>

</html>