<?php include_once 'dbconfig.php'; 
if(isset($_GET['delete_post'])){
	$post_id = $_GET['delete_post'];
    $sql="DELETE FROM posts WHERE id = $post_id";
    if ($conn->query($sql) === TRUE) {
        echo '<div class="status-modal">Deleted!</div>';
	} else {
        echo '<div class="status-modal">Didn not!</div>';
	}
}
if(isset($_GET['delete_slide'])){
	$slide_id = $_GET['delete_slide'];
    $sql="DELETE FROM slides WHERE id = $slide_id";
    if ($conn->query($sql) === TRUE) {
        echo '<div class="status-modal">Deleted!</div>';
	} else {
        echo '<div class="status-modal">Didn not!</div>';
	}
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="uHub">
    <meta name="robots" content="index,follow,noodp">
    <meta name="googlebot" content="index,follow">
    <meta property="og:locale" content="fa_IR">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="uHub">
    <meta name="theme-color" content="dark">
    <title>UHub - Dashboard</title>
    <link type="text/css" rel="stylesheet" href="static/plugins/materialize/css/materialize.min.css"/>
    <link href="static/css/materialIcons.css" rel="stylesheet">
    <link href="static/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
    <link href="static/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="static/plugins/slick/slick.css">
    <link rel="stylesheet" href="static/plugins/slick/slick-theme.css">
    <!-- Custom CSS -->
    <link href="static/css/uhub-custom.css" rel="stylesheet" type="text/css"/>
    <!--Theme-->
    <link href="static/themes/dark_pink.css" rel="stylesheet" type="text/css"/>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="color:white" class="dark-custom">
    <div>
        <h4>Posts</h4>
            <div class="btn dark-pink waves-effect waves-light"><a href="post.php">New Post</a></div>
            <table>
                <tr>
                    <td>title</td>
                    <td>edit</td>
                    <td>delete</td>
                </tr>
                <?php include_once 'dbconfig.php';
                    $q="SELECT * FROM posts";
                    $result=$conn->query($q);
                    if($result->num_rows > 0){
                    while($row = $result->fetch_assoc()){
                ?>
                <tr>
                    <td><a href="<?php echo $row['post_type'] ?>.php?<?php echo $row['post_type'] ?>=<?php echo $row['link'] ?>"><?php echo $row['title'] ?></a></td>
                    <td><a href="postedit.php?id=<?php echo $row['id'] ?>">Edit</a></td>
                    <td><a href="dashboard.php?delete_post=<?php echo $row['id'] ?>">Delete</a></td>
                </tr>
                <?php } } ?>
            </table>
        </div>
    <hr>
    <div>
    <h4>Slides</h4>
	    <div class="btn dark-pink waves-effect waves-light"><a href="slide.php">New Slide</a></div>
        <table>
            <tr>
                <td>title</td>
                <td>edit</td>
                <td>delete</td>
            </tr>
            <?php include_once 'dbconfig.php';
                $q="SELECT * FROM slides";
                $result=$conn->query($q);
                if($result->num_rows > 0){
                    while($row = $result->fetch_assoc()){
            ?>
            <tr>
                <td><a href="<?php echo $row['link'] ?>"><?php echo $row['caption'] ?></a></td>
                <td><a href="slideedit.php?id=<?php echo $row['id'] ?>">Edit</a></td>
                <td><a href="dashboard.php?delete_slide=<?php echo $row['id'] ?>">Delete</a></td>
            </tr>
            <?php } } ?>
        </table>
    </div>
</body>
</html>