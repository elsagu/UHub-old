-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 27, 2017 at 07:28 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uhub`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `title` varchar(64) COLLATE utf8_bin NOT NULL,
  `author` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `file` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin NOT NULL,
  `post_type` varchar(16) COLLATE utf8_bin NOT NULL,
  `link` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`title`, `author`, `image`, `file`, `content`, `post_type`, `link`, `id`, `post_time`) VALUES
('tag test', 'RH226', '99608-photo_x.jpg', '72830-', '<p>tag test</p>\r\n', 'book', 'tag-test', 1, '2017-04-27 07:46:05'),
('tag test2', 'RH226', '83157-photo_x.jpg', '57068-', '<p>tag test</p>\r\n', 'book', 'tag-test2', 2, '2017-04-27 07:46:29'),
('tag test222', 'Erfan aka', '6232-uhub-poster.jpg', '57984-', '<p>tag test222</p>\r\n', 'book', 'tag-test222', 3, '2017-04-27 10:52:36'),
('tag test233', 'Erfan aka', '54277-uhub-poster.jpg', '75598-', '<p>tag test222</p>\r\n', 'book', 'tag-test233', 4, '2017-04-27 10:53:10'),
('Book by erfan aka', 'RH226', '48745-photo_x.jpg', '89825-', '<p>Book by erfan aka</p>\r\n', 'book', 'book-by-erfan-aka', 5, '2017-04-27 11:33:23'),
('editing on work', 'Erfan aka', '60694-uhub-poster.jpg', '14000-', '', 'video', 'editing-on-work', 6, '2017-04-27 11:35:34'),
('Book by Johnny sins', 'RH226', '83263-photo_x.jpg', '2078-', '', 'video', 'book-by-johnny-sins', 7, '2017-04-27 11:35:56'),
('Test post by erfan', 'Vladimir Nabokov', '12003-photo_x.jpg', '83695-', '', 'book', 'test-post-by-erfan', 8, '2017-04-27 11:36:30'),
('testing on tags', 'RH226', '17326-3.jpg', '42104-', '<p>testing on tags</p>\r\n', 'video', 'testing-on-tags', 9, '2017-04-27 19:08:05'),
('tag test again', 'Vladimir Nabokov', '40575-1.jpg', '16106-', '', 'book', 'tag-test-again', 10, '2017-04-27 19:10:41'),
('tag test again and again', 'Vladimir Nabokov', '88499-1.jpg', '82423-', '', 'video', 'tag-test-again-and-again', 11, '2017-04-27 19:11:07'),
('tag test again and again and again', 'Vladimir Nabokov', '68310-photo_2017-02-11_19-53-58.jpg', '75407-', '', 'book', 'tag-test-again-and-again-and-again', 12, '2017-04-27 19:12:26');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_bin NOT NULL,
  `review` text COLLATE utf8_bin NOT NULL,
  `rating` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `name`, `review`, `rating`, `post_id`, `post_time`) VALUES
(1, 'saba', 'great ass! :O', 5, 2, '2017-04-27 08:34:05'),
(2, 'saba', 'Review', 1, 2, '2017-04-27 09:13:11'),
(3, 'saba', 'Review', 1, 2, '2017-04-27 09:13:51'),
(4, 'saba', 'ssssssss', 1, 2, '2017-04-27 09:14:07'),
(5, 'erfan akhavan', 's Error: ', 1, 2, '2017-04-27 09:15:06'),
(6, 'Red Head :(', 'Review ajax!', 1, 2, '2017-04-27 09:29:55');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `caption` varchar(64) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `link` varchar(64) COLLATE utf8_bin NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `image`, `caption`, `description`, `link`, `post_time`) VALUES
(1, '55929-slide-77.jpg', 'test slide edit', 'slide testing', 'test-slide-edit', '2017-04-26 09:40:12'),
(2, '85402-slide-climbing-948x640.jpg', 'test slide 2', 'slide testing 2', 'test-slide-2', '2017-04-24 22:55:28'),
(3, '72803-slide-17676.png', 'test slide23', 'test slide23', '/wwe-waad', '2017-04-24 23:03:27'),
(4, '92284-slide-photo_x.jpg', 'This is a slide', 'WOW ASS!', 'google.com', '2017-04-26 11:19:40');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tag_id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(255) COLLATE utf8_bin NOT NULL,
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag_id`, `tag`, `post_id`) VALUES
(1, 'litrature', 5),
(2, 'general', 5),
(3, 'linguistics', 5),
(4, 'litrature', 6),
(5, 'linguistics', 7),
(6, 'general', 8),
(7, 'linguistics', 8),
(8, 'general', 9),
(9, 'story', 9),
(10, 'story', 10),
(11, 'book', 10),
(12, 'story', 11),
(13, 'book', 11),
(14, 'story', 12),
(15, 'book', 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
