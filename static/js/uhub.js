/**
 * Created by alphonse on 4/18/2017.
 */
function close_sidenav(){
    $(".chat-sidebar").sideNav("hide");
}
$( document ).ready(function() {
    $('.carousels').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: false,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ]
    });
    var img = $('#image')[0];
    if (img){
        var colorThief = new ColorThief();
        var x = colorThief.getColor(img);
        $('#ctcontainer').css('background-color', 'rgb(' + x + ')');
    }

});

window.onload = function() {
    setTimeout(function(){
        $('body').addClass('loaded').trigger("loaded");
    }, 1000);
    setTimeout(function(){
        $('.loader').fadeOut('400');
    }, 600);
}

function showResult(str) {
  if (str.length==0) {
    document.getElementById("livesearch").innerHTML="";
	document.getElementById("livesearch").style.display="none";
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      document.getElementById("livesearch").innerHTML=this.responseText;
      document.getElementById("livesearch").style.display="block";
    }
  }
  xmlhttp.open("GET","search.php?q="+str,true);
  xmlhttp.send();
}

//Stops search submit with enter in both mobile&desktop
$('#search').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

//Post cover preview
function readURL(input) {
	if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    reader.onload = function (e) {
	        $('#preview').attr('src', e.target.result);
	    }
	    reader.readAsDataURL(input.files[0]);
	}
}
$("#cover").change(function(){
	readURL(this);
});

//Loads textarea content to ckeditor
function loadtext(){
	var x = document.getElementById('editor1').innerTEXT;
	CKEDITOR.instances['editor1'].setData(x);
}

//Ajax call to submit reviews
$("#review").submit(function(e) {
    var url = "review.php";
    $.ajax({
           type: "POST",
           url: url,
           data: $(this).serialize(), // serializes the form's elements.
           success: function(data)
           {
               alert('posted');
           }
	});
    e.preventDefault(); // avoid to execute the actual submit of the form.
});