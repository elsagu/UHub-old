<register>
    <div class="mn-content valign-wrapper">
        <main class="mn-inner container">
            <div class="valign">
                <div class="row">
                    <div class="col s12 m6 l4 offset-l4 offset-m3">
                        <div id="register-box" ref="register_box" class="card white darken-1 hoverable">
                            <div class="card-content ">
                                <span class="card-title right" 4>ثبت نام در عکس گرام</span>
                                <div class="row">
                                    <form action="/register/" method="POST" onchange="{ check_from_on_change }"
                                          onsubmit="{ check_register_form }" class="col s12">
                                        <input type="hidden" name="csrfmiddlewaretoken" ref="csrf_token" value="">
                                        <input if="{android}" type="hidden" name="android" value="true">
                                        <div class="input-field col s12">
                                            <input id="username-register" ref="username" name="username" type="text"
                                                   class="{invalid: invalids.username}">
                                            <label class="right-align" for="username-register">نام کاربری</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input id="email-register" ref="email_register" name="email" type="email"
                                                   class="{invalid: invalids.email}">
                                            <label class="right-align " for="email-register">ایمیل</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input ref="password_register" id="password-register" name="password"
                                                   type="password" class="{invalid: invalids.password}">
                                            <label class="right-align" for="password-register">رمز عبور</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input ref="password_2_register" id="password2-register" type="password"
                                                   class="{invalid: invalids.password_2}">
                                            <label class="right-align" for="password2-register">تکرار رمز عبور</label>
                                        </div>
                                        <div if="{ login_ajax_error }" class="row right-align">
                                            <div class="col s12 m12">
                                                <div class="card-panel red">
                                                    <span dir="auto" class="right-align white-text"><b>{login_ajax_error_msg}</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col s12 right-align m-t-sm">
                                            <a id="login_button" class="bold2-button waves-effect waves-light btn-flat">ورود</a>
                                            <button type="submit" id="register-but"
                                                    class="bold2-button waves-effect waves-light btn teal">ثبت نام
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <!--suppress JSAnnotator -->
    <script>
        this.invalids = {}
        this.items = opts.items
        var self = this;
        this.on('before-mount', function () {
            if (mounted_boxes) {
                mounted_boxes.unmount(true)
            }
            mounted_boxes = this;
        })
        this.on("mount", function () {
            if (this.items) {
                if (this.items.android) {
                    this.android = true
                }
            }
            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }

            this.refs.csrf_token.value = getCookie('csrftoken');
            self.update()
            $(document).ready(function () {
                $("title").html("عکس گرام - ثبت نام");
                $('body').on("loaded", function () {
                    $('#register-box').hide();
                    $('#register-box').delay(300).show("drop", {direction: "down"}, "slow")
                });
                $('#register-box').hide();
                $('#register-box').show("drop", {direction: "down"}, "slow")
                $("#login_button").click(function () {
                    route("/login/")
                })
            })
        });
        function isValidEmailAddress(emailAddress) {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            return pattern.test(emailAddress);
        }
        ;
        function update_form() {
            self.invalids.password = self.refs.password_register.value == '';
            if (self.refs.password_register.value.length < 8) {
                self.invalids.password = true
            }
            self.invalids.username = self.refs.username.value == '';
            if (self.refs.username.value.length < 3) {
                self.invalids.username = true
            }
            self.invalids.email = self.refs.email_register.value == '';
            if (!isValidEmailAddress(self.refs.email_register.value)) {
                self.invalids.email = true
            }
            self.invalids.password_2 = self.refs.password_2_register.value == '';
            if (self.refs.password_register.value != self.refs.password_2_register.value) {
                self.invalids.password_2 = true
            }
            var is_valid = true;
            for (var key in self.invalids) {
                if (self.invalids[key] == true) {
                    is_valid = false;
                }
            }
            return is_valid
        }
        check_from_on_change(e)
        {
            var is_valid = update_form();
            self.update();
            if (!is_valid) {
                return false
            }

        }
        check_register_form(e)
        {
            var is_valid = update_form();
            self.update()
            if (!is_valid) {
                e.preventDefault()
                $(self.refs.register_box).effect('shake');
                return false
            }
            $.ajax({
                url: "/api/username_validator",
                data: {
                    "email": self.refs.email_register.value,
                    "username": self.refs.username.value,
                },
                method: "POST",
                async: false,
                success: function (res) {
                    if (res["OK"] == true) {
                        return true
                    } else {
                        for (var i = 0; i < res["fields"].length; i++) {
                            self.invalids[res.fields[i]] = true
                        }
                        self.login_ajax_error_msg = "کاربری با این ایمیل/نام کاربری وجود دارد";
                        self.login_ajax_error = true
                        e.preventDefault()
                        self.update()
                        $(self.refs.register_box).effect('shake');
                        return false
                    }
                },
                fail: function () {
                }
            })
        }

    </script>

</register>

<login>
    <div class="mn-content valign-wrapper">
        <main class="mn-inner container">
            <div class="valign">
                <div class="row">
                    <div class="col s12 m6 l4 offset-l4 offset-m3">
                        <div id="login-box" ref="login_box" class="card white darken-1 hoverable">
                            <div class="card-content ">
                                <span class="card-title right">ورود به عکس گرام</span>
                                <div class="row">
                                    <form ref="login_form" onsubmit="{ login_f }" onchange="{ check_login_form }"
                                          class="col s12">
                                        <div class="input-field col s12">
                                            <input id="username" ref="username" type="text" name="username" dir="auto"
                                                   class="{invalid: invalids.username}">
                                            <label class="right-align" for="username">نام کاربری</label>
                                        </div>
                                        <div class="input-field col s12 ">
                                            <input ref="pass" id="password" name="password" type="password"
                                                   class="{invalid: invalids.password}">
                                            <label class="right-align" for="password">رمز عبور</label>
                                        </div>
                                        <div if="{ login_ajax_error }" class="row">
                                            <div class="col s12 m12">
                                                <div class="card-panel red">
                                                    <span class="white-text">{login_ajax_error_msg}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col s12 right-align m-t-sm">
                                            <a id="register_button"
                                               class="bold2-button waves-effect waves-light btn-flat">ثبت نام</a>
                                            <button id="login-but" type="submit"
                                                    class="bold2-button waves-effect waves-light btn teal">ورود
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script>
        this.invalids = {}
        var self = this;
        this.items = opts.items
        this.on('before-mount', function () {
            if (mounted_boxes) {
                mounted_boxes.unmount(true)
            }
            mounted_boxes = this;
        })
        this.on("mount", function () {
            $(document).ready(function () {
                $("title").html("عکس گرام - ورود");
                $('body').on("loaded", function () {
                    $('#login-box').hide();
                    $('#login-box').delay(300).show("drop", {direction: "down"}, "slow")
                });
                $('#login-box').hide();
                $('#login-box').show("drop", {direction: "down"}, "slow")
                $("#register_button").click(function () {
                    route("/register/")
                })
            })
        });


        login_f(e)
        {
            e.preventDefault()
            this.invalids.password = this.refs.pass.value == ''
            if (this.refs.pass.value.length < 8) {
                this.invalids.password = true
            }
            this.invalids.username = this.refs.username.value == ''
            if (this.refs.username.value.length < 3) {
                this.invalids.username = true
            }
            if (this.invalids.username || this.invalids.password) {
                $(this.refs.login_box).effect('shake');
                return false
            }
            var form = new FormData();
            form.append("username", this.refs.username.value);
            form.append("password", this.refs.pass.value);
            $.ajax({
                data: form,
                url: "/api/log_in",
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: false,
                success: function (result, status, xhr) {
                    if (result["OK"] == true) {
                        swal({
                            title: "! ورود شما موفقیت امیز بود",
                            text: "! به عکس گرام خوش امدید",
                            type: "success",
                            closeOnConfirm: false,
                            showConfirmButton: false,
                            timer: 2000
                        });
                        if (nav) {
                            nav.fetch_user()
                        }
                        self.unmount(true);
                        route("/gallery/")

                    } else {
                        self.login_ajax_error = true;
                        self.login_ajax_error_msg = result["status"];
                        self.invalids.username = true;
                        self.invalids.password = true;
                        self.update();
                        $(self.refs.login_box).effect('shake');
                    }
                }
            });
        }
        check_login_form(e)
        {
            this.invalids.password = this.refs.pass.value == '';
            if (this.refs.pass.value.length < 8) {
                this.invalids.password = true
            }
            this.invalids.username = this.refs.username.value == '';
            if (this.refs.username.value.length < 3) {
                this.invalids.username = true
            }
            self.update()
        }
    </script>


</login>

<nav-box>
    <div id="theme_modal" class="modal bottom-sheet">
        <div class="modal-content">
            <div class="row">
                <div onclick="{set_theme_to}" each="{ themes_list }" class="col s12 m6 l4">
                    <div image_id="{code}" style="{'background-color: ' + theme_select_modal_color + ' !important'}"
                         class="card boxes hoverable">
                        <div class="card-image">

                            <img style="height: 100% !important" src="{'/static/images/' + code + '_theme_preview.jpg'}"
                                 class="axgram-img">

                            <a style="{'background-color: ' + theme_select_button_color + ' !important'}"
                               class="btn-floating halfway-fab waves-effect waves-light "><i
                                    class="material-icons image-more-icon">check</i>
                            </a>

                        </div>
                        <div class="card-content">
                            <br>
                            <p dir="auto" style="{'color: ' + theme_select_text_color + ' !important'}"
                               class=" truncate">{persian_name}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="waves-effect modal-close waves-green btn-flat">بستن</button>
        </div>
    </div>
    <header class="mn-header navbar-fixed">
        <nav class="{opts.items.theme.nav}">
            <div class="nav-wrapper row ">
                <div class="header-title col s3">
                    <span style="font-weight: bolder" class="chapter-title"><b>AXGRAM</b></span>
                </div>
                <form class="left search col s6 hide-on-small-and-down">
                    <div class="input-field">
                        <input id="search" style="font-size: 200px;line-height: 3;" class="right-align right" dir="auto"
                               type="search" placeholder="جستجو" autocomplete="off">
                    </div>
                    <a href="javascript: void(0)" class="close-search"><i class="material-icons">close</i></a>
                </form>
                <ul class="right col s9 m3 nav-right-menu">
                    <li><a href="javascript:void(0)" data-activates="slide-out" data-activates-lol="chat-sidebar"
                           class="chat-button show-on-large"><i class="material-icons"
                                                                id="slide-out-icon">menu</i></a>
                    </li>
                    <li if="{user}" class=""><a href="javascript:void(0)" data-activates="dropdown1"
                                                class="dropdown-button dropdown-right show-on-large"><i
                            class="material-icons">notifications_none</i><span
                            class="badge">4</span></a></li>
                    <li class="hide-on-med-and-up"><a href="javascript:void(0)" class="search-toggle"><i
                            class="material-icons">search</i></a></li>
                </ul>
                <ul id="dropdown1" class=" {opts.items.theme.class} dropdown-content notifications-dropdown">
                    <li class="notificatoins-dropdown-container">
                        <ul>
                            <li class="notification-drop-title">Today</li>
                            <li>
                                <a href="#!">
                                    <div class="notification">
                                        <div class="notification-icon circle cyan"><i class="material-icons">done</i>
                                        </div>
                                        <div class="notification-text"><p><b>Alan Grey</b> uploaded new theme</p><span>7 min ago</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#!">
                                    <div class="notification">
                                        <div class="notification-icon circle deep-purple"><i class="material-icons">cached</i>
                                        </div>
                                        <div class="notification-text"><p><b>Tom</b> updated status</p>
                                            <span>14 min ago</span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#!">
                                    <div class="notification">
                                        <div class="notification-icon circle red"><i class="material-icons">delete</i>
                                        </div>
                                        <div class="notification-text"><p><b>Amily Lee</b> deleted account</p><span>28 min ago</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#!">
                                    <div class="notification">
                                        <div class="notification-icon circle cyan"><i
                                                class="material-icons">person_add</i></div>
                                        <div class="notification-text"><p><b>Tom Simpson</b> registered</p><span>2 hrs ago</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#!">
                                    <div class="notification">
                                        <div class="notification-icon circle green"><i class="material-icons">file_upload</i>
                                        </div>
                                        <div class="notification-text"><p>Finished uploading files</p>
                                            <span>4 hrs ago</span></div>
                                    </div>
                                </a>
                            </li>
                            <li class="notification-drop-title">Yestarday</li>
                            <li>
                                <a href="#!">
                                    <div class="notification">
                                        <div class="notification-icon circle green"><i
                                                class="material-icons">security</i></div>
                                        <div class="notification-text"><p>Security issues fixed</p>
                                            <span>16 hrs ago</span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#!">
                                    <div class="notification">
                                        <div class="notification-icon circle indigo"><i class="material-icons">file_download</i>
                                        </div>
                                        <div class="notification-text"><p>Finished downloading files</p>
                                            <span>22 hrs ago</span></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#!">
                                    <div class="notification">
                                        <div class="notification-icon circle cyan"><i class="material-icons">code</i>
                                        </div>
                                        <div class="notification-text"><p>Code changes were saved</p>
                                            <span>1 day ago</span></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>
        </nav>
    </header>
    <aside id="slide-out" class="side-nav {opts.items.theme.class_2}">
        <div class="side-nav-wrapper">
            <div if="{user}" class="sidebar-profile">
                <div class="sidebar-profile-image right-align">
                    <img src="{user.avatar? user.avatar : '/static/assets/images/profile-image-2.png'}" class="circle"
                         alt="">
                </div>
                <div class="sidebar-profile-info">
                    <a href="javascript:void(0);" class="account-settings-link">
                        <p style="" dir="auto" class="right right-align">{user.first_name} {user.last_name}</p>
                        <span class="white-text-custom">@{user.username}</span>
                    </a>
                </div>
            </div>
            <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
                <li onclick="{close_sidnav}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="/"><span
                        style="margin-right:-60px;font-size:110%"><b class="">صفحه اصلی</b></span><i
                        class="material-icons right">home</i></a></li>
                <li onclick="{close_sidnav}" if="{!user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="/register"><span
                        style="margin-right:-80px;font-size:110%"><b class="">ثبت نام</b></span><i
                        class="material-icons right">person_add</i></a></li>
                <li onclick="{close_sidnav}" if="{!user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="/login"><span
                        style="margin-right:-80px;font-size:110%"><b class="">ورود</b></span><i
                        class="material-icons right">input</i></a></li>
                <li onclick="{close_sidnav}" if="{user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="/profile"><span
                        style="margin-right:-80px;font-size:110%"><b class="">پروفایل</b></span><i
                        class="material-icons right">account_circle</i></a></li>
                <li onclick="{close_sidnav}" if="{user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="/settings"><span
                        style="margin-right:-80px;font-size:110%"><b class="">تنظیمات</b></span><i
                        class="material-icons right">settings</i></a></li>
                <li onclick="{close_sidnav}" if="{user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="/gallery"><span
                        style="margin-right:-80px;font-size:110%"><b style="padding-left: 10px"
                                                                     class="">گالری</b></span><i
                        class="material-icons right">collections</i></a></li>
                <li onclick="{close_sidnav}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="/images/new"><span
                        style="margin-right:-30px;font-size:110%"><b style="padding-left: 10px"
                                                                     class="">عکس های جدید</b></span><i
                        class="material-icons right">fiber_new</i></a></li>
                <li onclick="{close_sidnav}" if="{user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="/images/following"><span
                        style="margin-right:-30px;font-size:110%"><b style="padding-left: 10px"
                                                                     class="">عکس های دوستان</b></span><i
                        class="material-icons right">group</i></a></li>
                <li onclick="{close_sidnav}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="/images/top"><span
                        style="margin-right:-30px;font-size:110%"><b style="padding-left: 10px"
                                                                     class="">عکس های برتر</b></span><i
                        class="material-icons right">stars</i></a></li>
                <li onclick="{close_sidnav}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="/images/top_new"><span
                        style="margin-right:-30px;font-size:110%"><b style="padding-left: 10px"
                                                                     class="">بهترین های جدید</b></span><i
                        class="material-icons right">whatshot</i></a></li>
                <li onclick="{close_sidnav}" class="no-padding center {opts.items.theme.class_2}"><a
                        onclick="Materialize.toast('! به زودی', 4000)" class="waves-effect waves-light"
                        href=""><span
                        style="margin-right:-80px;font-size:110%"><b style="padding-right: 15px" class="">مرکز آموزش</b></span><i
                        class="material-icons right">local_library</i></a></li>
                <li onclick="{open_theme_modal}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"><span

                        style="margin-right:-40px;font-size:110%"><b style="padding-left: 15px"
                                                                     class="">انتخاب پوسته</b></span><i
                        class="material-icons right">color_lens</i></a></li>

            </ul>
            <div class="footer">
                <p class="copyright">Bamboo ©</p>
                <a href="">Privacy</a> &amp; <a href="">Terms</a>
            </div>
        </div>
    </aside>
    <script>
        this.items = opts.items
        var self = this;
        this.items.theme = get_theme()
        var nav_themes = []
        for (theme in themes_list) {
            nav_themes.push(themes_list[theme])
        }
        this.themes_list = nav_themes
        this.update()
        open_theme_modal(e)
        {
            $("#theme_modal").modal()
            $("#theme_modal").modal("open")
        }
        set_theme_to(e)
        {
            set_theme(e.item.code)
        }
        close_sidnav(e)
        {
            $(".chat-sidebar").sideNav("hide")
        }
        fetch_user()
        {
            $.ajax(
                {
                    url: "/api/self",
                    success: function (res) {
                        self.user = res;
                        user_obj = res
                        if (mounted_boxes) {
                            mounted_boxes.user_obj = user_obj
                            mounted_boxes.update()
                        }
                        self.update()
                    },
                    fail: function (res) {
                        self.user = false;
                        self.update()
                    }
                }
            )
        }
        this.on("mount", function () {
            this.fetch_user()
            nav = this
            $(document).ready(function () {
                console.log(self.opts.items.theme.color)
                $("meta[name=theme-color]").attr("content", self.opts.items.theme.color)
                $("#slide-out-icon").click(function () {
                    $("#slide-out-icon").fadeTo(100, 0);
                    $("#slide-out-icon").text("menu");
                    $("#slide-out-icon").fadeTo(100, 1);
                })
                $.getScript("/static/assets/js/alpha.js")
                $.getScript("/static/bower_components/material-preloader/js/materialPreloader.min.js")
                $("#materialPreloader").remove()

            })
        })

    </script>
</nav-box>

<footer-box>

    <div class="page-footer {opts.items.theme.class_2}">
        <div class="footer-grid container">
            <div class="footer-l {opts.items.theme.class_2}">&nbsp;</div>
            <div class="footer-grid-l {opts.items.theme.class_2}">
                <!--<a class="footer-text" href="layout-hidden-sidebar.html">-->
                <!--<i class="material-icons arrow-l">arrow_back</i>-->
                <!--<span class="direction">Previous</span>-->
                <!--<div>-->
                <!--Hidden Sidebar-->
                <!--</div>-->
                <!--</a>-->
            </div>
            <div class="footer-r {opts.items.theme.class_2}">&nbsp;</div>
            <div class="footer-grid-r {opts.items.theme.class_2}">
                <!--<a class="footer-text" href="form-elements.html">-->
                <!--<i class="material-icons arrow-r">arrow_forward</i>-->
                <!--<span class="direction">Next</span>-->
                <!--<div>-->
                <!--Form Elements-->
                <!--</div>-->
                <!--</a>-->
            </div>
        </div>
    </div>
    <script>
        var self = this
        this.items = opts.items
        this.on("mount", function () {
            footer = this
        })
    </script>

</footer-box>

<settings-box>
    <div class="row">
        <div class="col s12 m6">
            <div class="card boxes">
                <div class="card-content">
                    <span class="card-title right-align">عکس پروفایل</span><br>
                    <div class="row">
                        <div if="{user_obj.avatar}" class="col s12 m12">
                            <div class="card">
                                <div class="card-content  center center-align">
                                    <img style="max-width: 100%;" id="avatar_image" if="{user_obj.avatar}" ref="avatar"
                                         src="{user_obj.avatar}"
                                         class="center z-depth-3">
                                </div>
                                <div class="card-action">
                                    <a style="cursor:pointer" onclick="{ crop_init }"><i
                                            class="material-icons image-action-box-icon">content_cutr</i></a>
                                    <a style="cursor:pointer" onclick="{ delete_avatar }"><i
                                            class="material-icons image-action-box-icon right">delete_forever</i></a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <form ref="upload_avatar_form" onsubmit="{save_avatar}" class="col s12 m12">
                            <div class="row">
                                <div class="file-field input-field">
                                    <div class="btn {opts.items.theme.class}">
                                        <span>انتخاب عکس</span>
                                        <input name="image" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>

                            <button type="submit" style="margin-bottom: -20px" id="register-but"
                                    class="right bold2-button waves-effect waves-light btn {opts.items.theme.class}">
                                آپلود
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m6">
            <div class="card boxes">
                <div class="card-content">
                    <span class="card-title right-align">ویرایش اطلاعات شخصی</span><br>
                    <div class="row">
                        <form ref="personal_info_form" onsubmit="{save_personal_info}" class="col s12 m12">
                            <div class="row">
                                <div class="input-field col s12 m12 l6">
                                    <i class="material-icons prefix">account_circle</i>
                                    <input dir="auto" id="last-name" ref="last_name_input"
                                           riot-value={user_obj.last_name} name="last_name" type="text" class="">
                                    <label class="right-align" for="last-name">نام خانوادگی</label>
                                </div>
                                <div class="input-field col s12 m12 l6">
                                    <i class="material-icons prefix">account_circle</i>
                                    <input dir="auto" riot-value={user_obj.first_name} class="right-align"
                                           id="first-name" name="first_name" type="text">
                                    <label class="right-align" for="first-name">نام</label>
                                </div>
                                <div class="input-field col s12 m12 l12">
                                    <i class="material-icons prefix">email</i>
                                    <input dir="auto" riot-value={user_obj.email} class="left-align" id="email"
                                           name="email" type="email">
                                    <label class="right-align" for="email">ایمیل</label>
                                </div>
                                <div class="input-field col s12 m12 l12">
                                    <i class="material-icons prefix">person</i>
                                    <input dir="ltr" riot-value={user_obj.username} class="left-align" id="username"
                                           name="username" type="text">
                                    <label class="right-align" for="username">نام کاربری</label>
                                </div>

                                <div class="col s12 m12">
                                    <br>
                                    <button type="submit" style="margin-bottom: -20px" id="register-but"
                                            class="right bold2-button waves-effect waves-light btn {opts.items.theme.class}">
                                        ذخیره
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m6">
            <div class="card boxes">
                <div class="card-content">
                    <span class="card-title right-align">شبکه های اجتماعی</span><br>
                    <div class="row">
                        <form ref="social_media_form" onsubmit="{save_social_media_info}" class="col s12 m12">
                            <div class="row">
                                <div class="input-field col s12 m12 l12">
                                    <i class="fa fa-instagram prefix" aria-hidden="true"></i>
                                    <input dir="auto" id="instagram" ref="instgram_input"
                                           riot-value={user_obj.instagram} name="instagram" type="text" class="">
                                    <label class="right-align" for="instagram">اینستاگرام</label>
                                </div>
                                <div class="input-field col s12 m12 l12">
                                    <i class="fa fa-facebook prefix" aria-hidden="true"></i>
                                    <input dir="auto" id="facebook" ref="facebook_input"
                                           riot-value={user_obj.facebook} name="facebook" type="text" class="">
                                    <label class="right-align" for="facebook">فیسبوک</label>
                                </div>
                                <div class="input-field col s12 m12 l12">
                                    <i class="fa fa-snapchat prefix" aria-hidden="true"></i>
                                    <input dir="auto" id="snapchat" ref="snapchat_input"
                                           riot-value={user_obj.snapchat} name="snapchat" type="text" class="">
                                    <label class="right-align" for="snapchat">اسنپ چت</label>
                                </div>
                                <div class="input-field col s12 m12 l12">
                                    <i class="fa fa-twitter prefix" aria-hidden="true"></i>
                                    <input dir="auto" id="twitter" ref="twitter_input"
                                           riot-value={user_obj.twitter} name="twitter" type="text" class="">
                                    <label class="right-align" for="twitter">توییتر</label>
                                </div>
                                <div class="input-field col s12 m12 l12">
                                    <i class="fa fa-linkedin prefix" aria-hidden="true"></i>
                                    <input dir="auto" id="linkedin" ref="linkedin_input"
                                           riot-value={user_obj.linkedin} name="linkedin" type="text" class="">
                                    <label class="right-align" for="linkedin">لینکدین</label>
                                </div>
                                <div class="input-field col s12 m12 l12">
                                    <i class="fa fa-pinterest prefix" aria-hidden="true"></i>
                                    <input dir="auto" id="pinterest" ref="pinterest_input"
                                           riot-value={user_obj.pinterest} name="pinterest" type="text" class="">
                                    <label class="right-align" for="pinterest">پینترست</label>
                                </div>
                                <div class="input-field col s12 m12 l12">
                                    <i class="fa fa-deviantart prefix" aria-hidden="true"></i>
                                    <input dir="auto" id="deviantart" ref="deviantart_input"
                                           riot-value={user_obj.deviantart} name="deviantart" type="text" class="">
                                    <label class="right-align" for="deviantart">دویان ارت</label>
                                </div>
                                <div class="col s12 m12">
                                    <br>
                                    <button type="submit" style="margin-bottom: -20px" id="register-but"
                                            class="right bold2-button waves-effect waves-light btn {opts.items.theme.class}">
                                        ذخیره
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var self = this;
        self.user_obj = user_obj
        settings_box = self
        this.items = opts.items
        this.on('before-mount', function () {
            self.is_avatar_updated = true
            self.fetch_user()
            if (mounted_boxes) {
                mounted_boxes.unmount(true)
            }
            mounted_boxes = this;
        })
        this.on("mount", function () {
            $("title").html("عکس گرام - تنظیمات");
            animate_boxes()
            this.fetch_user()

        })
        this.on("unmount", function () {
            settings_box = false;
            jcrop_used = false
            jcrop_init = false

            if (
                jcrop_obj
            ) {
                jcrop_obj.destroy(), jcrop_obj = false
            }
        })

        crop_init(e)
        {
            if (!jcrop_used) {
                swal("ابتدا بخشی از عکس را برای بریدن انتخاب کنید")
                if (!jcrop_obj) {
                    $(self.refs.avatar).Jcrop({
                        onSelect: updateCoords,
                        onChange: updateCoords,
                        onRelease: delete_crods,
                        aspectRatio: 1,
                        minSize: [80, 80],
                        touchSupport: true,
                        trueSize: [$(self.refs.avatar)[0].naturalWidth, $(self.refs.avatar)[0].naturalHeight],
                    }, function () {
                        jcrop_init = false
                        jcrop_obj = this
                    });
                }
                return
            }
            $.ajax({
                url: "/api/crop_avatar",
                data: jcrop_used,
                method: "POST",
                success: function () {
                    self.is_avatar_updated = true
                    self.fetch_user()
                    swal({
                        title: 'عکس شما با موفقیت بریده شد',
                        text: ' ',
                        timer: 2000,
                        type: "success",
                        showConfirmButton: false,
                    })
                }
            })

        }

        delete_avatar(e)
        {
            e.preventDefault()
            $.ajax({
                url: "/api/delete_avatar",
                method: "POST",
                processData: false,
                contentType: false,
                success: function () {
                    self.fetch_user()
                    swal({
                        title: 'عکس شما با موفقیت حذف شد',
                        text: ' ',
                        timer: 2000,
                        type: "success",
                        showConfirmButton: false,
                    })
                }
            })
        }

        save_avatar(e)
        {
            e.preventDefault()
            $.ajax({
                url: "/api/upload_avatar",
                data: new FormData($(self.refs.upload_avatar_form)[0]),
                method: "POST",
                processData: false,
                contentType: false,
                success: function () {
                    self.is_avatar_updated = true
                    self.fetch_user()
                    swal({
                        title: 'عکس شما با موفقیت آپلود شد',
                        text: ' ',
                        timer: 2000,
                        type: "success",
                        showConfirmButton: false,
                    })
                }
            })
        }
        save_social_media_info(e)
        {
            e.preventDefault()
            $.ajax({
                url: "/api/user/" + user_obj.id.toString() + "/",
                data: new FormData($(self.refs.social_media_form)[0]),
                method: "PUT",
                processData: false,
                contentType: false,
                success: function (res) {
                    self.fetch_user()
                    swal({
                        title: '.اطلاعات با موفقیت ذخیره شد',
                        text: ' ',
                        timer: 2000,
                        type: "success",
                        showConfirmButton: false,
                    })
                },
                error: function (jqXHR, exception, text) {
                    var swal_option;
                    swal_option = {
                        title: ' ',
                        text: ' ',
                        timer: 3000,
                        type: "error",
                        showConfirmButton: false,
                    };
                    if (jqXHR.status === 400) {
                        var error_obj = jQuery.parseJSON(jqXHR.responseText)[0];
                        if (error_obj == "email_taken") {
                            swal_option["title"] = ".این ایمیل قبلا در سیستم ثبت شده"
                        } else if (error_obj == "username_taken") {
                            swal_option["title"] = ".این نام کاربری قبلا در سیستم ثبت شده"
                        }
                        swal(swal_option)
                    }

                },
            })
        }
        save_personal_info(e)
        {
            e.preventDefault()
            $.ajax({
                url: "/api/user/" + user_obj.id.toString() + "/",
                data: new FormData($(self.refs.personal_info_form)[0]),
                method: "PUT",
                processData: false,
                contentType: false,
                success: function (res) {
                    self.fetch_user()
                    swal({
                        title: '.اطلاعات با موفقیت ذخیره شد',
                        text: ' ',
                        timer: 2000,
                        type: "success",
                        showConfirmButton: false,
                    })
                },
                error: function (jqXHR, exception, text) {
                    var swal_option;
                    swal_option = {
                        title: ' ',
                        text: ' ',
                        timer: 3000,
                        type: "error",
                        showConfirmButton: false,
                    };
                    if (jqXHR.status === 400) {
                        var error_obj = jQuery.parseJSON(jqXHR.responseText)[0];
                        if (error_obj == "email_taken") {
                            swal_option["title"] = ".این ایمیل قبلا در سیستم ثبت شده"
                        } else if (error_obj == "username_taken") {
                            swal_option["title"] = ".این نام کاربری قبلا در سیستم ثبت شده"
                        }
                        swal(swal_option)
                    }

                },
            })
        }
        this.on("updated", function () {
            if (self.is_avatar_updated) {
                if (jcrop_obj) {
                    jcrop_obj.destroy()
                }
                $('#avatar_image').attr("style", "max-width: 100%;").removeAttr("src").attr("src", self.user_obj.avatar + "?timestamp=" + new Date().getTime())
                if (self.user_obj.avatar) {
                    if (!jcrop_init) {
                        jcrop_init = true
                        function updateCoords(c) {
                            jcrop_used = {
                                "x": c.x,
                                "y": c.y,
                                "x2": c.x2,
                                "y2": c.y2
                            }
                        };
                        delete_crods = function () {
                            jcrop_used = false
                        }
                        $(self.refs.avatar).load(function () {
                            $(self.refs.avatar).Jcrop({
                                onSelect: updateCoords,
                                onChange: updateCoords,
                                onRelease: delete_crods,
                                aspectRatio: 1,
                                minSize: [80, 80],
                                touchSupport: true,
                                trueSize: [$(self.refs.avatar)[0].naturalWidth, $(self.refs.avatar)[0].naturalHeight],
                            }, function () {
                                jcrop_init = false
                                jcrop_obj = this
                            });
                        })
                    }


                }
                self.is_avatar_updated = false
            }

        })

        fetch_user()
        {
            $.ajax(
                {
                    url: "/api/self",
                    success: function (res) {
                        self.user_obj = res;
                        nav.user = res
                        nav.update()
                        user_obj = res
                        self.update()
                    },
                    error: function (res) {
                        self.user_obj = false;
                        user_obj = false
                        self.update()

                    }
                }
            )
        }
        this.on("updated", function () {
            Materialize.updateTextFields();
        })
    </script>
</settings-box>

<profile-box>
    <div class="row">
        <div class="col m3 s1 hide-on-med-and-down">
        </div>
        <div class="col m6 s12">
            <div class="card center-align" style="margin-top:65px">
                <img style="margin-top:-60px"
                     src="{user_obj.avatar? user_obj.avatar : '/static/assets/images/profile-image-2.png'}"
                     class="responsive-img circle center" width="128px" alt="">
                <div class="card-content">
                    <p dir="auto" class="m-t-lg flow-text"><b>{user_obj.first_name} {user_obj.last_name}</b></p>
                    <p class="m-t-lg flow-text tooltipped" data-position="top" data-delay="50"
                       data-tooltip="<span style='font-size: 130%'>ایدی شما در عکس گرام</span>">{user_obj.id}</p>
                    <p class="m-t-lg flow-text tooltipped" data-position="top" data-delay="50"
                       data-tooltip="<span style='font-size: 130%'>نام کاربری شما</span>">@{user_obj.username}</p>
                </div>
            </div>
        </div>
        <div class="col m3 s1 hide-on-med-and-down">
        </div>
    </div>
    <script>
        var self = this;
        this.items = opts.items
        this.on('before-mount', function () {
            if (mounted_boxes) {
                mounted_boxes.unmount(true)
            }
            mounted_boxes = this;
        })
        this.on("mount", function () {
            if (nav) {
                nav.fetch_user()
            }
            animate_boxes()
            $("[data-tooltip]").tooltip({delay: 50, html: true})
            $(".comment-decs").each(function () {
                $(this).html(next_line_fix($(this).html()))
            })
            $('p').each(function () {
                $(this).html(linkHashtags($(this).html()));
                $(this).html(linkMentions($(this).html()));
            });
            this.update()
        })
        this.on("updated", function () {
            $("[data-tooltip]").tooltip({delay: 50, html: true})
            $(".comment-decs").each(function () {
                $(this).html(next_line_fix($(this).html()))
            })
            $('p').each(function () {
                $(this).html(linkHashtags($(this).html()));
                $(this).html(linkMentions($(this).html()));
            });
        })
    </script>
</profile-box>
<index-box>
    <h1 id="index-title" class="header center">عکس گرام</h1>
    <br>
    <div class="row center">
        <h5 id="index-desc" class="header col s12 light">اجتماعی مجازی، قدرت گرفته جهت میزبانی تصاویر شما در برابر
            هزاران چشم</h5>
    </div>
    <br>
    <div class="row center">
        <a id="start-button" class="btn-large waves-effect waves-light {opts.items.theme.class}">شروع</a>
    </div>
    <br><br>
    <script>
        var self = this;
        this.items = opts.items
        this.on('before-mount', function () {
            if (mounted_boxes) {
                mounted_boxes.unmount(true)
            }
            mounted_boxes = this;
        })
        this.on("mount", function () {
            $('.button-collapse').sideNav();
            $('.parallax').parallax();
            animate_boxes()
            $("[data-tooltip]").tooltip({delay: 50, html: true})
            $(".comment-decs").each(function () {
                $(this).html(next_line_fix($(this).html()))
            })
            $('p').each(function () {
                $(this).html(linkHashtags($(this).html()));
                $(this).html(linkMentions($(this).html()));
            });
        })
    </script>
</index-box>
<image-box>
    <div ref="comment_modal" id="comment_modal" class="modal modal-fixed-footer">
        <div class="modal-content">
            <div if="{user_obj}" class="row">


                <div class="card hoverable">
                    <h4 class="right-align right send-comment-title">ارسال کامنت</h4>
                    <form onsubmit="{submit_comment}" ref="add_comment_form" enctype="multipart/form-data">
                        <input type="hidden" name="image_id" value="{comment_next_id}">
                        <div class="card-content rtl">
                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea data-length="1000" dir="auto" name="text" id="comment-text-box"
                                              class="materialize-textarea"></textarea>
                                    <label for="comment-text-box">متن کامنت</label>
                                    <br if="{add_comment_image}">
                                    <br if="{add_comment_image}">
                                </div>
                            </div>
                            <div if="{add_comment_image}" class="row">
                                <div class="file-field input-field">
                                    <div class="btn {opts.items.theme.class}">
                                        <span>اضافه کردن عکس</span>
                                        <input name="comment_picture" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-action">
                            <a style="cursor:pointer">
                                <button type="submit" id="completed-task" style="  background: none;
  padding: 0px;
  border: none;"><i data-position="top" data-delay="50"
                    data-tooltip="<span style='font-size: 130%'>ارسال</span>"
                    class="material-icons image-action-box-icon">send</i></button>
                            </a>
                            <a class="{none: add_comment_image}" style="cursor:pointer"><i onclick="{add_comment_photo}"
                                                                                           data-position="top"
                                                                                           data-delay="50"
                                                                                           data-tooltip="<span style='font-size: 130%'>اضافه کردن عکس</span>"
                                                                                           class="material-icons right image-action-box-icon">insert_photo</i></a>
                        </div>
                    </form>
                </div>


            </div>
            <div if="{user_obj}" class="divider grey darken-3"></div>
            <div class="row">
                <h4 class="right">کامنت ها</h4>
                <div each="{ comments }" ref="{ 'comments_' + id }" class="col s12 m12 l12">
                    <div comment_id="{id}" class="card hoverable">
                        <div class="card-content rtl">
                            <img src="{user.avatar? user.avatar : '/static/assets/images/profile-image-2.png'}"
                                 class="{golden_user_image: user.verified} circle image-comment-user-avatar" alt="">
                            <span class="card-comment-username">@{user.username} <i if="{user.is_developer}"
                                                                                    data-position="top" data-delay="50"
                                                                                    data-tooltip="<span style='font-size: 130%'>برنامه نویس عکس گرام</span>"
                                                                                    class="material-icons dev_user_icon">code</i></span>
                            <i data-position="top" data-delay="50"
                               data-tooltip="<span style='font-size: 130%'>کاربر تایید شده</span>"
                               if="{user.verified && !user.is_developer}"
                               class="material-icons golden_user_icon">check_circle</i>
                            <br>
                            <img if={pic_download_link} ref="image_full" src="{pic_download_link}"
                                 class="axgram-full-image center">
                            <br>
                            <p dir="auto" class="comment-decs">{text}</p>
                        </div>
                        <div class="card-action">
                            <a onclick="{like_comment}" style="cursor:pointer"><i data-position="top" data-delay="50"
                                                                                  data-tooltip="<span style='font-size: 130%'>لایک کردن کامنت</span>"
                                                                                  class="material-icons image-action-box-icon"
                                                                                  ref="{ 'like_comment_icon_' + id }">{
                                is_liked_by_user ? 'favorite' : 'favorite_border'}</i><span
                                    class="action-number" ref="{ 'like_comment_number_' + id }">{likes_count}</span></a>
                            <a onclick="{comment_reply}" style="cursor:pointer"><i data-position="top" data-delay="50"
                                                                                   data-tooltip="<span style='font-size: 130%'>پاسخ</span>"
                                                                                   class="material-icons image-action-box-icon">reply</i></a>
                            <a if="{is_user_owner}" onclick="{delete_comment}" style="cursor:pointer"><i
                                    data-position="top"
                                    data-delay="50"
                                    data-tooltip="<span style='font-size: 130%'>حذف کردن کامنت</span>"
                                    class="tooltipped material-icons right image-action-box-icon">delete_forever</i></a>
                            <a if="{is_user_owner}" style="cursor:pointer"><i data-position="top" data-delay="50"
                                                                              data-tooltip="<span style='font-size: 130%'>پین کردن کامنت</span>"
                                                                              class="tooltipped image-action-box-icon material-icons right">turned_in</i></a>
                        </div>
                    </div>
                </div>
                <div if="{comment_next}" class="col s12 m12 center">
                    <br>
                    <br>
                    <button ref="next_comment_button" onclick="{comment_init}"
                            class="btn-floating btn-large waves-effect waves-light {opts.items.theme.class} tooltipped"
                            data-position="right" data-delay="50"
                            data-tooltip="<span style='font-size: 130%'> ... بیشتر</span>"><i ref="next_button_icon"
                                                                                              class="material-icons">autorenew</i>
                    </button>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="waves-effect modal-close waves-green btn-flat">بستن</button>
        </div>
    </div>
    <div class="row" if="{image}">
        <div class="col s12 m12">
            <div class="card">
                <div class="card-content  center center-align">
                    <img if={image} ref="image_full" src="{image.download_url}" class="axgram-full-image hoverable">
                </div>
                <div class="card-content">
                    <p dir="auto" class="image-decs image-desc-single">{image.description}</p>
                </div>
                <div class="card-action">
                    <a onclick="{like_image}" style="cursor:pointer"><i class="material-icons image-action-box-icon"
                                                                        ref="like_icon"
                    >{image.is_liked_by_user ? 'favorite' : 'favorite_border'}</i><span ref="like_count"
                                                                                        class="action-number">{image.likes_count}</span></a>
                    <a onclick="{comment_init}" style="cursor:pointer"><i class="material-icons image-action-box-icon">comment</i>
                        <span
                                class="action-number">{image.comments_count}</span></a>
                    <a onclick="{ copy_link }" style="cursor:pointer"><i
                            class="material-icons image-action-box-icon right">link</i></a>
                    <a if="{!image.is_user_owner}" style="cursor:pointer"><i
                            class="material-icons image-action-box-icon right">report</i></a>
                    <a if="{image.is_user_owner}" style="cursor:pointer" onclick="{ delete_image }"><i
                            class="material-icons image-action-box-icon right">delete_forever</i></a>
                </div>
            </div>

        </div>
    </div>
    <div class="row" if="{image}">
        <div class="col s12 m6 white-text-custom">
            <ul class="collapsible hoverable" data-collapsible="expandable">
                <li>
                    <div class="collapsible-header"><i class="material-icons right collapsible-icon">camera_alt</i><span
                            class="right collapsible-text">دوربین</span></div>
                    <div class="collapsible-body">
                        <if cond="{image.camera}">
                            <ul class="collection">
                                <li if="{image.camera.name}" class="collection-item"><span
                                        class="right axgram-collection-title">مدل دوربین</span> <span>{image.camera.name}</span>
                                </li>
                                <li if="{image.camera.manufacturer}" class="collection-item"><span
                                        class="right axgram-collection-title">شرکت سازنده</span> <span>{image.camera.manufacturer}</span>
                                </li>
                                <li if="{image.meta_tag.exif.LensModel}" class="collection-item"><span
                                        class="right axgram-collection-title">مدل لنز</span> <span>{image.meta_tag.exif.LensModel}</span>
                                </li>
                            </ul>

                        </if>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i
                            class="material-icons right collapsible-icon">location_on</i><span
                            class="right collapsible-text">منطقه</span></div>
                    <div class="collapsible-body">
                        <div if="{image.location}" id="map"></div>
                        <ul if="{image.location}" class="collection">
                            <li if="{image.location.address.address.country}" class="collection-item"><span
                                    class="right axgram-collection-title">کشور</span> <span>{image.location.address.address.country}</span>
                            </li>
                            <li if="{image.location.address.address.state}" class="collection-item"><span
                                    class="right axgram-collection-title">استان</span> <span>{image.location.address.address.state}</span>
                            </li>
                            <li if="{image.location.address.address.county}" class="collection-item"><span
                                    class="right axgram-collection-title">شهر</span> <span>{image.location.address.address.county}</span>
                            </li>
                        </ul>
                    </div>

                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons right collapsible-icon">settings</i><span
                            class="right collapsible-text">تنظیمات دوربین</span></div>
                    <div class="collapsible-body">
                        <ul class="collection">
                            <li if="{image.meta_tag.exif.BrightnessValue}" class="collection-item"><span
                                    class="right axgram-collection-title">میزان روشنایی</span> <span>{image.meta_tag.exif.BrightnessValue}</span>
                            </li>
                            <li if="{image.meta_tag.exif.WhiteBalance}" class="collection-item"><span
                                    class="right axgram-collection-title">میزان سفیدی</span> <span>{image.meta_tag.exif.WhiteBalance}</span>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons right collapsible-icon">flash_on</i><span
                            class="right collapsible-text">فلش</span></div>
                    <div class="collapsible-body"></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons right collapsible-icon">date_range</i><span
                            class="right collapsible-text">زمان</span></div>
                    <div class="collapsible-body"></div>
                </li>
            </ul>
        </div>
        <div if="{image.likes.length > 0}" class="col s12 m6">
            <div class="card hoverable">
                <div class="card-content white-text">
                    <span class="card-title right-align">لایک ها</span>
                    <div class="row">
                        <div each="{image.likes}" class="chip">
                            <img src="{avatar? avatar : '/static/assets/images/profile-image-2.png'}"
                                 alt="Contact Person">@{username}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        var self = this
        this.items = opts.items
        this.user_obj = user_obj
        this.update()
        this.on('before-mount', function () {
            if (mounted_boxes) {
                mounted_boxes.unmount(true)
            }
            mounted_boxes = this;
        })
        animate_boxes()
        copy_link(e)
        {
            copyTextToClipboard(window.location.origin + "/i/" + self.image.id)
            Materialize.toast("! لینک این عکس کپی شد", 2000)
        }
        like_image(e)
        {
            $.ajax({
                url: "/api/like_unlike_image",
                method: "POST",
                data: {id: self.image.id},
                success: function (res, status) {
                    if (status === "success" && res["OK"] === true) {
                        self.image = res["image"]
                        self.update()
                    }
                },
                statusCode: {
                    403: function () {
                        route("/login/");
                    },
                }
            })

        }
        this.on("update", function () {
            if (this.image) {
                if (this.image.likes_count > 1000) {
                    round_int()
                }
            }
        })

        delete_image()
        {
            swal({
                title: 'آیا از پاک کردن این عکس مطمئن هستید ؟',
                text: " ",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#8f000c',
                cancelButtonColor: '#2c2b31',
                cancelButtonText: 'نه نگهش دار',
                confirmButtonText: '! آره پاکش کن'
            }).then(function () {
                $.ajax({
                    url: "/api/i/" + self.image.id,
                    method: "DELETE",
                    success: function () {
                        route("/gallery/")
                    }
                })
                swal(
                    'عکس مورد نظر پاک شد',
                    ' ',
                    'success'
                )

            })
        }

        fetch_image()
        {
            $.ajax({
                url: "/api/i/" + self.items.image_id,
                method: "GET",
                dataType: "json",
                cache: "false",
                jsonpCallback: "onJSONPLoad",
                error: function (jqXHR, exception) {
                    if (jqXHR.status === 0) {
                        //alert('Not connect.\n Verify Network.'); TODO: Clean up
                    } else if (jqXHR.status == 404) {
                        self.unmount()
                        route("/images/new/")
                    }
                },
                StatusCode: {
                    404: function () {
                        self.unmout()
                        route("/images/new/")
                        console.log("lol")
                    }
                },
                success: function (res) {
                    $("title").html("عکس شماره " + res["id"]);
                    if ("meta_tag" in res) {
                        res["meta_tag"] = fix_meta_data(res["meta_tag"])
                    }
                    self.image = res;
                    self.update()
                    $(".image-decs").html(next_line_fix($(".image-decs").html()))
                    $('p').each(function () {
                        $(this).html(linkHashtags($(this).html()));
                        $(this).html(linkMentions($(this).html()));
                    });
                    $.getScript("/static/js/utils.js")
                    if (self.image.location) {
                        var mapOptions = {
                            center: new google.maps.LatLng(parseFloat(res["meta_tag"]["location"]["lat"]), parseFloat(res["meta_tag"]["location"]["lon"])),
                            zoom: 10
                        };
                        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
                    }

                }
            })
        }
        add_comment_photo()
        {
            self.add_comment_image = true
            self.update()
        }
        comment_reply(e)
        {
            $('#comment-text-box').val($('#comment-text-box').val() + " @" + e.item.user.username);
        }
        submit_comment(e)
        {
            Materialize.toast("<span style='font-size: 130%'>درحال ارسال کامنت</span>", 3000)
            e.preventDefault()
            $.ajax({
                url: "/api/add_image_comment",
                data: new FormData($(self.refs.add_comment_form)[0]),
                method: "POST",
                processData: false,
                contentType: false,
                success: function (res) {
                    if (res["OK"] == true) {
                        self.comment_next = false
                        self.add_comment_image = false
                        self.comments = false
                        self.comment_init({item: {id: self.comment_next_id}})
                    } else {

                    }
                },
                fail: function () {
                }
            })
        }

        comment_init(e)
        {
            $(self.refs.comment_modal).modal("open")
            if (!self.comment_next) {
                self.comment_next_id = self.image.id
            }

            var url = ""
            var id = 0;
            if (self.comment_next) {
                url = self.comment_next
                id = self.comment_next_id
            } else {
                url = "/api/get_image_comments?limit=2&ordering=-date_created"
                id = self.image.id
            }
            $.ajax({
                url: url,
                method: "GET",
                data: {id: id},
                success: function (res, status) {
                    if (self.comment_next) {
                        if (res["next"]) {
                            self.comment_next = res["next"]
                            self.update()
                        } else {
                            self.comment_next = false
                            self.update()
                        }
                        for (var i = 0; i < res["results"].length; i++) {
                            self.comments.push(res["results"][i])
                        }
                        self.update();
                        round_int()
                        $('p').each(function () {
                            $(this).html(linkHashtags($(this).html()));
                            $(this).html(linkMentions($(this).html()));
                        });
                    } else {
                        if (status == "success") {
                            if (res["next"]) {
                                self.comment_next = res["next"]
                                self.comment_next_id = self.image.id
                                self.update()
                                $(self.refs["next_comment_button_icon"]).hover(
                                    function () {
                                        $(this).addClass("fa-spin");
                                    }, function () {
                                        $(this).removeClass("fa-spin");
                                    }
                                );
                            }
                            self.comments = res["results"]
                            self.update();
                            round_int()
                        } else {
                            route("/login/")
                        }
                    }

                }
            })
        }

        delete_comment(e)
        {
            swal({
                title: 'آیا از پاک کردن این کامنت مطمئن هستید ؟',
                text: " ",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#8f000c',
                cancelButtonColor: '#2c2b31',
                cancelButtonText: 'نه نگهش دار',
                confirmButtonText: '! آره پاکش کن'
            }).then(function () {
                $.ajax({
                    url: "/api/get_image_comment/" + e.item.id + "/",
                    method: "DELETE",
                    success: function () {
                        $(self.refs['comments_' + e.item.id]).hide()
                        Materialize.toast("<span style='font-size: 130%'>کامنت مورد نظر پاک شد.</span>", 3000)
                    }
                })


            })
        }
        this.on("updated", function () {
            $("[data-tooltip]").tooltip({delay: 50, html: true})
            $(".comment-decs").each(function () {
                $(this).html(next_line_fix($(this).html()))
            })
            $('p').each(function () {
                $(this).html(linkHashtags($(this).html()));
                $(this).html(linkMentions($(this).html()));
            });
        })
        this.on("mount", function () {
            $(self.refs.comment_modal).modal({
                dismissible: false,
                complete: function () {
                    if ($(self.refs.add_comment_form).length)         // use this if you are using class to check
                    {
                        $(self.refs.add_comment_form)[0].reset()
                    }

                    self.comments = false
                    self.comment_next = false
                    self.comment_next_id = false
                    self.add_comment_image = false
                    self.update()
                    self.fetch_image()
                }
            })
            this.fetch_image()
            gallery_temp_image = self.items.image_id
        })
    </script>
</image-box>

<images-box>
    <!-- Modal Structure -->
    <div id="upload" class="modal">
        <div class="modal-content">
            <h4 class="right">آپلود عکس</h4>
            <br><br>
            <form ref="upload_photo_form" class="col s12" enctype="multipart/form-data">
                <div class="row">
                    <div dir="rtl" class="file-field rtl input-field">
                        <div class="btn {opts.items.theme.class} rtl">
                            <span>انتخاب عکس</span>
                            <input name="image" type="file">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                    <div class="input-field col s12">
                        <textarea dir="auto" name="description" id="desc-box" class="materialize-textarea"></textarea>
                        <label for="desc-box">توضیحات</label>
                    </div>
                </div>
            </form>
        </div>
        <div class="progress white" id="upload-progress-box" style="display: none">
            <div class="determinate" id="upload-progress"></div>
        </div>
        <div class="modal-footer">
            <button type="submit" id="upload-photo" class="waves-effect waves-green btn-flat">آپلود</button>
        </div>
    </div>

    <div ref="comment_modal" id="comment_modal" class="modal modal-fixed-footer">
        <div class="modal-content">
            <div if="{user_obj}" class="row">


                <div class="card hoverable">
                    <h4 class="right-align right send-comment-title">ارسال کامنت</h4>
                    <form onsubmit="{submit_comment}" ref="add_comment_form" enctype="multipart/form-data">
                        <input type="hidden" name="image_id" value="{comment_next_id}">
                        <div class="card-content rtl">
                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea data-length="1000" dir="auto" name="text" id="comment-text-box"
                                              class="materialize-textarea"></textarea>
                                    <label for="comment-text-box">متن کامنت</label>
                                    <br if="{add_comment_image}">
                                    <br if="{add_comment_image}">
                                </div>
                            </div>
                            <div if="{add_comment_image}" class="row">
                                <div class="file-field input-field">
                                    <div class="btn {opts.items.theme.class}">
                                        <span>اضافه کردن عکس</span>
                                        <input name="comment_picture" type="file">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-action">
                            <a style="cursor:pointer">
                                <button type="submit" id="completed-task" style="  background: none;
  padding: 0px;
  border: none;"><i data-position="top" data-delay="50"
                    data-tooltip="<span style='font-size: 130%'>ارسال</span>"
                    class="material-icons image-action-box-icon">send</i></button>
                            </a>
                            <a class="{none: add_comment_image}" style="cursor:pointer"><i onclick="{add_comment_photo}"
                                                                                           data-position="top"
                                                                                           data-delay="50"
                                                                                           data-tooltip="<span style='font-size: 130%'>اضافه کردن عکس</span>"
                                                                                           class="material-icons right image-action-box-icon">insert_photo</i></a>
                        </div>
                    </form>
                </div>


            </div>
            <div if="{user_obj}" class="divider grey darken-3"></div>
            <div class="row">
                <h4 class="right">کامنت ها</h4>
                <div each="{ comments }" ref="{ 'comments_' + id }" class="col s12 m12 l12">
                    <div comment_id="{id}" class="card hoverable">
                        <div class="card-content rtl">
                            <img src="{user.avatar? user.avatar : '/static/assets/images/profile-image-2.png'}"
                                 class="{golden_user_image: user.verified} {dev_user_image: user.is_developer} circle image-comment-user-avatar"
                                 alt="">
                            <span class="card-comment-username">@{user.username} <i if="{user.is_developer}"
                                                                                    data-position="top" data-delay="50"
                                                                                    data-tooltip="<span style='font-size: 130%'>برنامه نویس عکس گرام</span>"
                                                                                    class="material-icons dev_user_icon">code</i></span>
                            <i data-position="top" data-delay="50"
                               data-tooltip="<span style='font-size: 130%'>کاربر تایید شده</span>"
                               if="{user.verified && !user.is_developer}"
                               class="material-icons golden_user_icon">check_circle</i>
                            <br>
                            <img if={pic_download_link} ref="image_full" src="{pic_download_link}"
                                 class="axgram-full-image">
                            <br>
                            <p dir="auto" class="comment-decs">{text}</p>
                        </div>
                        <div class="card-action">
                            <a onclick="{like_comment}" style="cursor:pointer"><i data-position="top" data-delay="50"
                                                                                  data-tooltip="<span style='font-size: 130%'>لایک کردن کامنت</span>"
                                                                                  class="material-icons image-action-box-icon"
                                                                                  ref="{ 'like_comment_icon_' + id }">{
                                is_liked_by_user ? 'favorite' : 'favorite_border'}</i><span
                                    class="action-number" ref="{ 'like_comment_number_' + id }">{likes_count}</span></a>
                            <a onclick="{comment_reply}" style="cursor:pointer"><i data-position="top" data-delay="50"
                                                                                   data-tooltip="<span style='font-size: 130%'>پاسخ</span>"
                                                                                   class="material-icons image-action-box-icon">reply</i></a>
                            <a if="{is_user_owner}" onclick="{delete_comment}" style="cursor:pointer"><i
                                    data-position="top"
                                    data-delay="50"
                                    data-tooltip="<span style='font-size: 130%'>حذف کردن کامنت</span>"
                                    class="tooltipped material-icons right image-action-box-icon">delete_forever</i></a>
                            <a if="{is_user_owner}" style="cursor:pointer"><i data-position="top" data-delay="50"
                                                                              data-tooltip="<span style='font-size: 130%'>پین کردن کامنت</span>"
                                                                              class="tooltipped image-action-box-icon material-icons right">turned_in</i></a>
                        </div>
                    </div>
                </div>
                <div if="{comment_next}" class="col s12 m12 center">
                    <br>
                    <br>
                    <button ref="next_comment_button" onclick="{comment_init}"
                            class="btn-floating btn-large waves-effect waves-light {opts.items.theme.class} tooltipped"
                            data-position="right" data-delay="50"
                            data-tooltip="<span style='font-size: 130%'> ... بیشتر</span>"><i ref="next_button_icon"
                                                                                              class="material-icons">autorenew</i>
                    </button>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="waves-effect modal-close waves-green btn-flat">بستن</button>
        </div>
    </div>

    <div class="fixed-action-btn">
        <a class="btn-floating waves-effect rtl right right-align waves-light btn-large red" id="uploadbtn">
            <i class="large material-icons">add</i>
        </a>
    </div>
    <div class="row">
        <div each="{ images }" ref="{ 'image_' + id }" class="col s12 m6 l4">
            <div image_id="{id}" class="card boxes hoverable">
                <div class="card-image">

                    <img src="{thumb_download_url}" data-zoom-image="{download_url}" class="axgram-img">

                    <a href="{'/i/' + id}" class="btn-floating halfway-fab waves-effect waves-light red"><i
                            class="material-icons image-more-icon">more_horiz</i>
                    </a>

                </div>
                <img src="{user.avatar? user.avatar : '/static/assets/images/profile-image-2.png'}"
                     class="circle {golden_user_image: user.verified} {dev_user_image: user.is_developer} image-user-avatar"
                     alt="">
                <span class="card-username">@{user.username} </span><i if="{user.is_developer}" data-position="top"
                                                                       data-delay="50"
                                                                       data-tooltip="<span style='font-size: 130%'>برنامه نویس عکس گرام</span>"
                                                                       class="material-icons dev_user_icon">code</i> <i
                    data-position="top" data-delay="50"
                    data-tooltip="<span style='font-size: 130%'>کاربر تایید شده</span>"
                    if="{user.verified && !user.is_developer}"
                    class="material-icons golden_user_icon">check_circle</i>
                <div class="card-content">
                    <br>
                    <p dir="auto" class="image-decs truncate">{description}</p>
                </div>
                <div class="card-action">
                    <a onclick="{like_image}" style="cursor:pointer"><i data-position="top" data-delay="50"
                                                                        data-tooltip="<span style='font-size: 130%'>لایک کردن</span>"
                                                                        class="material-icons image-action-box-icon"
                                                                        ref="{ 'like_image_icon_' + id }">{
                        is_liked_by_user ? 'favorite' : 'favorite_border'}</i><span
                            class="action-number" ref="{ 'like_image_number_' + id }">{likes_count}</span></a>
                    <a onclick="{comment_init}" style="cursor:pointer"><i data-position="top" data-delay="50"
                                                                          data-tooltip="<span style='font-size: 130%'>کامنت ها</span>"
                                                                          ref="next_button_icon"
                                                                          class="material-icons image-action-box-icon">comment</i>
                        <span
                                class="action-number">{comments_count}</span></a>
                    <a if="{!is_user_owner}" style="cursor:pointer"><i data-position="top" data-delay="50"
                                                                       data-tooltip="<span style='font-size: 130%'>گزارش</span>"
                                                                       ref="next_button_icon"
                                                                       class="material-icons image-action-box-icon right">report</i></a>
                    <a onclick="{ copy_link }" style="cursor:pointer"><i
                            data-position="top" data-delay="50"
                            data-tooltip="<span style='font-size: 130%'>کپی کردن لینک</span>" ref="next_button_icon"
                            class="material-icons image-action-box-icon right">link</i></a>
                </div>
            </div>
        </div>
        <div if="{next}" class="row">
            <div class="col s12 m12 center">
                <br>
                <br>
                <button ref="next_button" onclick="{fetch_images}"
                        class="btn-floating btn-large waves-effect waves-light {opts.items.theme.class} tooltipped"
                        data-position="right" data-delay="50"
                        data-tooltip="<span style='font-size: 130%'> ... بیشتر</span>"><i ref="next_comment_button_icon"
                                                                                          class="material-icons">autorenew</i>
                </button>
            </div>
        </div>

    </div>
    <script>
        this.items = opts.items
        this.comments = false
        this.user_obj = user_obj
        this.update()
        var self = this;

        this.on('before-mount', function () {
            if (mounted_boxes) {
                mounted_boxes.unmount(true)
            }
            mounted_boxes = this;
        })
        copy_link(e)
        {
            copyTextToClipboard(window.location.origin + "/i/" + e.item.id)
            Materialize.toast("! لینک این عکس کپی شد", 2000)
        }
        add_comment_photo()
        {
            self.add_comment_image = true
            self.update()
        }

        delete_comment(e)
        {
            swal({
                title: 'آیا از پاک کردن این کامنت مطمئن هستید ؟',
                text: " ",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#8f000c',
                cancelButtonColor: '#2c2b31',
                cancelButtonText: 'نه نگهش دار',
                confirmButtonText: '! آره پاکش کن'
            }).then(function () {
                $.ajax({
                    url: "/api/get_image_comment/" + e.item.id + "/",
                    method: "DELETE",
                    success: function () {
                        $(self.refs['comments_' + e.item.id]).hide()
                        Materialize.toast("<span style='font-size: 130%'>کامنت مورد نظر پاک شد.</span>", 3000)
                    }
                })


            })
        }

        like_image(e)
        {
            $.ajax({
                url: "/api/like_unlike_image",
                method: "POST",
                data: {id: e.item.id},
                success: function (res, status) {
                    if (status == "success" && res["OK"] == true) {
                        if (res["code"] == "image_liked") {
                            $(self.refs["like_image_icon_" + e.item.id.toString()]).html("favorite");
                            $(self.refs["like_image_number_" + e.item.id.toString()]).html(res["image"]["likes_count"].toString())
                            round_int()
                        } else {
                            $(self.refs["like_image_icon_" + e.item.id.toString()]).html("favorite_border");
                            $(self.refs["like_image_number_" + e.item.id.toString()]).html(res["image"]["likes_count"].toString())
                            round_int()
                        }
                    }
                },
                statusCode: {
                    403: function () {
                        route("/login/");
                    },
                }
            })

        }
        comment_reply(e)
        {
            $('#comment-text-box').val($('#comment-text-box').val() + " @" + e.item.user.username);
        }
        submit_comment(e)
        {
            Materialize.toast("<span style='font-size: 130%'>درحال ارسال کامنت</span>", 3000)
            e.preventDefault()
            $.ajax({
                url: "/api/add_image_comment",
                data: new FormData($(self.refs.add_comment_form)[0]),
                method: "POST",
                processData: false,
                contentType: false,
                success: function (res) {
                    if (res["OK"] == true) {
                        self.comment_next = false
                        self.add_comment_image = false
                        self.comments = false
                        self.comment_init({item: {id: self.comment_next_id}})
                    } else {

                    }
                },
                fail: function () {
                }
            })
        }

        comment_init(e)
        {
            $(self.refs.comment_modal).modal("open")
            console.log(e.item)
            if (!self.comment_next) {
                self.comment_next_id = e.item.id
            }

            var url = ""
            var id = 0;
            if (self.comment_next) {
                url = self.comment_next
                id = self.comment_next_id
            } else {
                url = "/api/get_image_comments?limit=2&ordering=-date_created"
                id = e.item.id
            }
            $.ajax({
                url: url,
                method: "GET",
                data: {id: id},
                success: function (res, status) {
                    if (self.comment_next) {
                        if (res["next"]) {
                            self.comment_next = res["next"]
                            self.update()
                        } else {
                            self.comment_next = false
                            self.update()
                        }
                        for (var i = 0; i < res["results"].length; i++) {
                            self.comments.push(res["results"][i])
                        }
                        self.update();
                        round_int()
                        $('p').each(function () {
                            $(this).html(linkHashtags($(this).html()));
                            $(this).html(linkMentions($(this).html()));
                        });
                    } else {
                        if (status == "success") {
                            if (res["next"]) {
                                self.comment_next = res["next"]
                                self.comment_next_id = e.item.id
                                self.update()
                                $(self.refs["next_comment_button_icon"]).hover(
                                    function () {
                                        $(this).addClass("fa-spin");
                                    }, function () {
                                        $(this).removeClass("fa-spin");
                                    }
                                );
                            }
                            self.comments = res["results"]
                            self.update();
                            round_int()
                        } else {
                            route("/login/")
                        }
                    }

                }
            })
        }

        fix_scroll()
        {
            if (gallery_temp_image) {
                try {
                    $('html, body').animate({
                        scrollTop: $(self.refs["image_" + gallery_temp_image.toString()]).offset().top
                    }, 500);
                } catch (e) {
                    console.log(e)
                }
            }

        }
        this.on("updated", function () {
            $("[data-tooltip]").tooltip({delay: 50, html: true})
            $(".comment-decs").each(function () {
                $(this).html(next_line_fix($(this).html()))
            })
            $('p').each(function () {
                $(this).html(linkHashtags($(this).html()));
                $(this).html(linkMentions($(this).html()));
            });
        })
        fetch_images()
        {
            var url = "";
            if (this.items.status === "gallery") {
                $("title").html("عکس گرام - گالری");
                url = "/api/self_images?ordering=-date_created&limit=9"
            } else if (this.items.status === "new") {
                $("title").html("عکس گرام - عکس های جدید");
                url = "/api/images?ordering=-date_created&limit=9"
            } else if (this.items.status === "top") {
                $("title").html("عکس گرام - عکس های برتر");
                url = "/api/images?ordering=-likes_count&limit=9"
            } else if (this.items.status === "following") {
                $("title").html("عکس گرام - عکس های دوستان");
                url = "/api/images?ordering=-date_created&limit=9&feed=true"
            } else if (this.items.status === "top_new") {
                $("title").html("عکس گرام - بهترین های جدید");
                url = "/api/images?ordering=-likes_count&limit=9"
            }
            if (self.next) {
                url = self.next
            }
            $.ajax({
                url: url,
                method: "GET",
                success: function (res, status) {
                    if (self.next) {
                        if (res["next"]) {
                            self.next = res["next"]
                            self.update()
                        } else {
                            self.next = false
                            self.update()
                        }
                        for (var i = 0; i < res["results"].length; i++) {
                            if (!res["results"][i].description) {
                                res["results"][i].description = "‌ "
                            }
                        }
                        $(".boxes").removeClass("boxes")
                        for (var i = 0; i < res["results"].length; i++) {
                            self.images.push(res["results"][i])
                        }
                        self.update();
                        animate_boxes();
                        round_int()
                        $('p').each(function () {
                            $(this).html(linkHashtags($(this).html()));
                            $(this).html(linkMentions($(this).html()));
                        });
                    } else {
                        if (status == "success") {
                            if (res["next"]) {
                                self.next = res["next"]
                                self.update()
                                $(self.refs["next_button_icon"]).hover(
                                    function () {
                                        $(this).addClass("fa-spin");
                                    }, function () {
                                        $(this).removeClass("fa-spin");
                                    }
                                );
                            }
                            for (var i = 0; i < res["results"].length; i++) {
                                if (!res["results"][i].description) {
                                    res["results"][i].description = "‌ "
                                }
                            }
                            self.images = res["results"]
                            self.update();
                            animate_boxes();
                            round_int()
                            $('p').each(function () {
                                $(this).html(linkHashtags($(this).html()));
                                $(this).html(linkMentions($(this).html()));
                            });
                            self.fix_scroll()
                        } else {
                            window.location = "/login"
                        }
                    }

                }
            })
        }
        this.on("update", function () {
            $(this.refs.next_button).tooltip({delay: 50, html: true});
        })
        this.on("mount", function () {
            $(self.refs.comment_modal).modal({
                dismissible: false,
                complete: function () {
                    if ($(self.refs.add_comment_form).length)         // use this if you are using class to check
                    {
                        $(self.refs.add_comment_form)[0].reset()
                    }

                    self.comments = false
                    self.comment_next = false
                    self.comment_next_id = false
                    self.add_comment_image = false
                    self.update()
                }
            })
            this.user_obj = user_obj
            this.update()
            this.fetch_images()
            $(document).ready(function () {

                $("#uploadbtn").click(function () {
                    $('#upload').modal('open');
                })
                $(".image-decs").each(function () {
                    //$(this).html(next_line_fix($(this).html()))
                })
            });
            $("#upload-photo").click(function () {
                // TODO : Fix bar after first upload
                $("#upload-progress-box").show()
                var $toastContent = $('<span style="font-size: 130%">درحال آپلود عکس</span>');
                Materialize.toast($toastContent, 5000);
                $.ajax({
                    // Your server script to process the upload
                    url: '/api/upload_image',
                    type: 'POST',

                    // Form data
                    data: new FormData($(self.refs.upload_photo_form)[0]),

                    // Tell jQuery not to process data or worry about content-type
                    // You *must* include these options! TODO: Clean up
                    cache: false,
                    contentType: false,
                    processData: false,
                    // Custom XMLHttpRequest
                    xhr: function () {
                        var myXhr = $.ajaxSettings.xhr();
                        if (myXhr.upload) {
                            // For handling the progress of the upload
                            myXhr.upload.addEventListener('progress', function (e) {
                                if (e.lengthComputable) {
                                    var load = e.loaded;
                                    var total = e.total;
                                    var percent = (load * 100) / total;
                                    if (percent == 100) {
                                        var $toastContent = $('<span style="font-size: 130%">درحال پردازش عکس</span>');
                                        Materialize.toast($toastContent, 3000);
                                        $("#upload-progress").removeClass("determinate").addClass("indeterminate").attr("style", "")

                                    } else {
                                        $("#upload-progress").attr("style", "width: " + percent.toString() + "%")
                                    }
                                }
                            }, false);
                        }
                        return myXhr;
                    },
                    success: function (res, status) {
                        if (status == "success" && res["OK"] == true) {
                            self.fetch_images();
                            var $toastContent = $('<span style="font-size: 130%">عکس به گالری شما اضافه شد.</span>');
                            Materialize.toast($toastContent, 5000);
                            $("#upload-progress-box").hide()
                            $('#upload').modal('close');
                        }
                    }
                });
            })
            $('#upload').modal({
                    dismissible: true,
                    opacity: .5,
                    inDuration: 300,
                    outDuration: 200,
                    startingTop: '4%',
                    endingTop: '10%',
                    ready: function (modal, trigger) {
                        //
                    }
                }
            );
        })
    </script>
</images-box>