;
(function($) {
    var defaults = {
        position: 'bottom',
        height: '5px',
        col_1: '#159756',
        col_2: '#da4733',
        col_3: '#3b78e7',
        col_4: '#fdba2c',
        fadeIn: 200,
        fadeOut: 200
    }
    $.materialPreloader = function(options) {
        var settings = $.extend({}, defaults, options);
        $template =
            "";
        $('body').prepend($template);
        this.on = function() {
            $('#materialPreloader').fadeIn(settings.fadeIn);
        }
        this.off = function() {
            $('#materialPreloader').fadeOut(settings.fadeOut);
        }
    }
}(jQuery));
