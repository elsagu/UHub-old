// Tags vars

var nav = false;
var footer = false;
var mounted_boxes = undefined;
var gallery_temp_image = undefined;
var settings_box = false;
var global_theme = themes_list["pink_dark"];
function set_theme(theme) {
    if (theme in themes_list) {
        Cookies.set("theme", theme)
        var _theme = themes_list[theme]
        $("meta[name=theme-color]").attr("content", _theme.color)
        $("#theme_css").attr("href", _theme.src)
        $("body").removeClass(global_theme._class_2).addClass(_theme.class_2)
        $("nav").removeClass(global_theme.nav).addClass(_theme.nav)
        global_theme = _theme
        if (mounted_boxes) {
            if (mounted_boxes.items) {
                mounted_boxes.items.theme = _theme
                mounted_boxes.update()
            }
        }
        if (footer){
            footer.items.theme = _theme
            footer.update()
        }
    }

}


/*

                    Routing system

*/

function mount_nav_footer(title, desc, theme) {
    footer = riot.mount("footer-box", {
        title: "footer",
        items: {
            page_title: title,
            description: desc,
            theme: theme
        }
    });

    nav = riot.mount("nav-box", {
        title: "nav",
        items: {
            page_title: title,
            description: desc,
            theme: theme
        }
    });
}
function unmount_nav_footer() {
    if (footer) {
        footer.unmount(true)
    }
    if (nav) {
        nav.unmount(true)
    }
}
route.base("/");
mount_nav_footer("گالری", "پروفایل عمومی شما", get_theme())
route("", function (name) {
    riot.mount("index-box", {
        title: "index",
        items: {
            theme: get_theme(),
            status: "index"
        }
    });
});
route("settings..", function (name) {
    riot.mount("settings-box", {
        title: "settings",
        items: {
            theme: get_theme()
        }
    });
});
route("gallery..", function (name) {
    riot.mount("images-box", {
        title: "gallery",
        items: {
            theme: get_theme(),
            status: "gallery"
        }
    });
});

route('login..', function (name) {
    if (Cookies.get('is_auth')) {
        route("/gallery");
        return true
    } else {
        riot.mount("login", {
            title: "login",
            items: {
                theme: get_theme()
            }
        });
    }

});

route('register..', function (name) {
    var q = route.query()
    if ("android" in q){
        riot.mount("register", {
            title: "register",
            items: {
                theme: get_theme(),
                android: true
            }
        }
        );
        return ""
    }
    if (Cookies.get('is_auth')) {
        route("/gallery");
        return true
    } else {
        riot.mount("register", {
            title: "register",
            items: {
                theme: get_theme(),
            }
        });
    }

});

route('/i/*/..', function (image) {
    riot.mount("image-box", {
        title: "image",
        items: {
            theme: get_theme(),
            image_id: image,
            query: route.query()
        }
    });
});
route('/i/*', function (image) {
    riot.mount("image-box", {
        title: "image",
        items: {
            theme: get_theme(),
            image_id: image,
            query: {}
        }
    });
});
route('/images/*/?..', function (status) {
    console.log(status)
    riot.mount("images-box", {
        title: "new_image",
        items: {
            theme: get_theme(),
            status: status,
            query: route.query()
        }
    });
});

route('/images/*', function (status) {
    console.log(status)
    riot.mount("images-box", {
        title: "new_image",
        items: {
            theme: get_theme(),
            status: status,
            query: {}
        }
    });
});
route("profile..", function (name) {
    riot.mount("profile-box", {
        title: "profile",
        items: {
            theme: get_theme(),
            status: "gallery",
            user: "self"
        }
    });
});

route.start(true);

