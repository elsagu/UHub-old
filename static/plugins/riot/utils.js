/**
 * Created by iman on 2/14/17.
 */

/*!
 * JavaScript Cookie v2.1.3
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
function copyTextToClipboard(text) {
    var textArea = document.createElement("textarea");

    //
    // *** This styling is an extra step which is likely not required. ***
    //
    // Why is it here? To ensure:
    // 1. the element is able to have focus and selection.
    // 2. if element was to flash render it has minimal visual impact.
    // 3. less flakyness with selection and copying which **might** occur if
    //    the textarea element is not visible.
    //
    // The likelihood is the element won't even render, not even a flash,
    // so some of these are just precautions. However in IE the element
    // is visible whilst the popup box asking the user for permission for
    // the web page to copy to the clipboard.
    //

    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';


    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        console.log('Oops, unable to copy');
    }

    document.body.removeChild(textArea);
}

(function (factory) {
    var registeredInModuleLoader = false;
    if (typeof define === 'function' && define.amd) {
        define(factory);
        registeredInModuleLoader = true;
    }
    if (typeof exports === 'object') {
        module.exports = factory();
        registeredInModuleLoader = true;
    }
    if (!registeredInModuleLoader) {
        var OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = OldCookies;
            return api;
        };
    }
}(function () {
    function extend() {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[i];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }

    function init(converter) {
        function api(key, value, attributes) {
            var result;
            if (typeof document === 'undefined') {
                return;
            }

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {
                }

                if (!converter.write) {
                    value = encodeURIComponent(String(value))
                        .replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
                } else {
                    value = converter.write(value, key);
                }

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires ? '; expires=' + attributes.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                    attributes.path ? '; path=' + attributes.path : '',
                    attributes.domain ? '; domain=' + attributes.domain : '',
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    var name = parts[0].replace(rdecode, decodeURIComponent);
                    cookie = converter.read ?
                        converter.read(cookie, name) : converter(cookie, name) ||
                        cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {
                        }
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {
                }
            }

            return result;
        }

        api.set = api;
        api.get = function (key) {
            return api.call(api, key);
        };
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }

    return init(function () {
    });
}));

var themes_list = {
    "pink_dark": {
        "code": "pink_dark",
        "src": "/static/themes/dark_pink.css",
        "persian_name": "صورتی - تیره",
        "color": "#1f1e23",
        "class": "pink darken-4",
        "class_2": "dark-custom",
        "nav": "dark-custom",
        "theme_select_button_color": "#880e4f",
        "theme_select_modal_color": "#2c2b31",
        "theme_select_text_color": "white",
    },
    "light_green": {
        "code": "light_green",
        "src": "/static/themes/light_green.css",
        "persian_name": "سبز - روشن",
        "color": "#388e3c",
        "class": "green darken-2",
        "class_2": "",
        "nav": "green darken-2",
        "theme_select_button_color": "#388e3c",
        "theme_select_modal_color": "white",
        "theme_select_text_color": "black",
    }
};
var global_theme = themes_list["pink_dark"];
var BOXES_ANIMATION = {
    effect: "drop",
    options: {"direction": "down"},
    duration: 300,
};

var jcrop_used = false;
var jcrop_init = false;
var jcrop_obj = false;

function get_boxes_animition() {
    return BOXES_ANIMATION
}

function get_theme() {
    if (Cookies.get("theme")) {
        try {
            var _theme = themes_list[Cookies.get("theme")]
            $("meta[name=theme-color]").attr("content", _theme.color)
            $("#theme_css").attr("href", _theme.src)
            $("body").addClass(_theme.class_2)
            global_theme = _theme
            return _theme
        } catch (e) {
            var _theme = themes_list["pink_dark"]
            $("meta[name=theme-color]").attr("content", _theme.color)
            $("#theme_css").attr("href", _theme.src)
            $("body").removeClass(global_theme._class_2).addClass(_theme.class_2)
            global_theme = _theme
            return _theme
        }

    } else {
        var _theme = themes_list["pink_dark"]
        $("meta[name=theme-color]").attr("content", _theme.color)
        $("#theme_css").attr("href", _theme.src)
        $("body").removeClass(global_theme._class_2).addClass(_theme.class_2)
        global_theme = _theme
        return _theme
    }
}

function animate_boxes() {
    var anim = get_boxes_animition();
    $('body').on("loaded", function () {
        $(".boxes").delay(1300).show(anim["effect"], anim["options"], anim["duration"])
    });
    $(".boxes").each(function (t) {
        $(this).toggle(anim["effect"], anim["options"], anim["duration"])
    })
}

var lastScrollTop = 0;
var action = "stopped";
var timeout = 100;
var user_obj = false;
// Scroll end detector:
$.fn.scrollEnd = function (callback, timeout) {
    $(this).scroll(function () {
        // get current scroll top
        var st = $(this).scrollTop();
        var $this = $(this);
        // fix for page loads
        if (lastScrollTop != 0) {
            // if it's scroll up
            if (st < lastScrollTop) {
                action = "scrollUp";
            }
            // else if it's scroll down
            else if (st > lastScrollTop) {
                action = "scrollDown";
            }
        }
        // set the current scroll as last scroll top
        lastScrollTop = st;
        // check if scrollTimeout is set then clear it
        if ($this.data('scrollTimeout')) {
            clearTimeout($this.data('scrollTimeout'));
        }
        // wait until timeout done to overwrite scrolls output
        $this.data('scrollTimeout', setTimeout(callback, timeout));
    });
};

$(window).scrollEnd(function () {
    if (action != "stopped") {
        //call the event listener attached to obj.
        $(document).trigger(action);
    }
}, timeout);

// scale scroll stuff
function scroll_scale() {
    $(document).on('scrollUp', function () {
        $('.fixed-action-btn').removeClass('scale-out');
        $('.fixed-action-btn').addClass('scale-in')
    });
    $(document).on('scrollDown', function () {
        $('.fixed-action-btn').removeClass('scale-in');
        $('.fixed-action-btn').addClass('scale-out')
    });

}


/////////// API STUFF ///////////

function get_usernames_tags() {
    var usernames = [];
    var tags = [];
    $.ajax({
        async: false,
        url: "/api/usernames_tags",
        method: "GET",
        success: function (res, status) {
            if (status == "success") {
                usernames = res.usernames;
                tags = res.tags
            }
        }
    })
}

function get_self_images() {
    var images = [];
    $.ajax({
        async: false,
        url: "/api/self_images",
        method: "GET",
        success: function (res, status) {
            if (status == "success") {
                images = res["results"]
            } else {
                window.location = "/login"
            }
        }
    });
    return images
}

function abbrNum(number, decPlaces) {
    var orig = number;
    var dec = decPlaces;
    // 2 decimal places => 100, 3 => 1000, etc
    decPlaces = Math.pow(10, decPlaces);

    // Enumerate number abbreviations
    var abbrev = ["k", "m", "b", "t"];

    // Go through the array backwards, so we do the largest first
    for (var i = abbrev.length - 1; i >= 0; i--) {

        // Convert array index to "1000", "1000000", etc
        var size = Math.pow(10, (i + 1) * 3);

        // If the number is bigger or equal do the abbreviation
        if (size <= number) {
            // Here, we multiply by decPlaces, round, and then divide by decPlaces.
            // This gives us nice rounding to a particular decimal place.
            var number = Math.round(number * decPlaces / size) / decPlaces;

            // Handle special case where we round up to the next abbreviation
            if ((number == 1000) && (i < abbrev.length - 1)) {
                number = 1;
                i++;
            }

            // console.log(number);
            // Add the letter for the abbreviation
            number += abbrev[i];

            // We are done... stop
            break;
        }
    }
    return number;
}


/////////// API STUFF ///////////

function usernames_tags_populate(element) {
    $.ajax({
        url: "/api/usernames_tags",
        method: "GET",
        success: function (res, status) {
            if (status == "success") {
                usernames = res.usernames;
                tags = res.tags;
                $(element).suggest('@', {
                    data: usernames,
                    map: function (user) {
                        return {
                            value: user.username,
                            text: '<strong>@' + user.username + '</strong>'
                        }
                    }
                })
            }
        }
    })
}

hashtag_regexp = /#([\u0600-\u06FFa-zA-Z][\u0600-\u06FFa-zA-Z0-9_\-\.]+)/g;

function linkHashtags(text) {
    return text.replace(
        hashtag_regexp,
        '<a class="hashtag" href="/tag/$1">#$1</a>'
    );
}

mention_regexp = /@([a-zA-Z0-9_\-\.]+)/g;

function linkMentions(text) {
    return text.replace(
        mention_regexp,
        '<a class="mention" href="/u/$1">@$1</a>'
    );
}

function next_line_fix(text) {
    return text.replace(/\n?([\S| \t | ]*)/g, "<p dir='auto' class='image-decs image-desc-single'>$1</p>")
}

function round_int() {
    $(".action-number").each(function () {
        var num = parseInt($(this).html(), 10);
        $(this).html(abbrNum(num, 1))
    })
}


$(document).ready(function () {
    $('.collapsible').collapsible();
    $('ul.tabs').tabs();
});

function fix_meta_data(meta) {
    if ("BrightnessValue" in meta.exif) {
        if (meta.exif["BrightnessValue"].includes("/")) {
            var vals = meta.exif["BrightnessValue"].split("/");
            meta.exif["BrightnessValue"] = Math.round(parseFloat(vals[0]) / parseFloat(vals[1]));
            meta.exif["BrightnessValue"] = meta.exif["BrightnessValue"].toString()
        }
    }
    console.log(meta);
    return meta
}