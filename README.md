# Project UHub

## Index
    includes slider, posts section, access menu
 
## Dashboard
    Lists Post&slide with delete and edit ability. Also button to new post&slide

## Post type Pages
    Different pages for Different types of posts. Its link structure will be like this: uhub.ir/videos

## Single Pages
    Pages which show contents of a post. Its link structure will be like this: uhub.ir/video/this-is-a-sample-post

## Tags Pages
    Lists of all tags. After choosing a tag, user will be redirected to tag page

## Tag Pages
    Shows the posts that have the specific tag