<?php include_once 'dbconfig.php' ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="uHub">
    <meta name="robots" content="index,follow,noodp">
    <meta name="googlebot" content="index,follow">
    <meta property="og:locale" content="fa_IR">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="uHub">
    <meta name="theme-color" content="dark">
    <title>{{ title }}</title>
    <link type="text/css" rel="stylesheet" href="static/plugins/materialize/css/materialize.min.css"/>
    <link href="static/css/materialIcons.css" rel="stylesheet">
    <link href="static/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
    <link href="static/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="static/plugins/slick/slick.css">
    <link rel="stylesheet" href="static/plugins/slick/slick-theme.css">
    <!-- Custom CSS -->
    <link href="static/css/uhub-custom.css" rel="stylesheet" type="text/css"/>
    <!--Theme-->
    <link href="static/themes/dark_pink.css" rel="stylesheet" type="text/css"/>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .search-box {
            padding: 5px;
            width: 100%;
            height: auto;
            background-color: #1f1e23;
            position: absolute;
            left: 0;
            top: 97%;
            display: none;
        }
    </style>
</head>
<body class="dark-custom">
<div class="loader-bg dark-custom"></div>
<div class="loader">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
        <div class="spinner-layer spinner-teal lighten-1">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
        <div class="spinner-layer spinner-yellow">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
        <div class="spinner-layer spinner-green">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
</div>
<div class="mn-content fixed-sidebar">
    <header class="mn-header navbar-fixed">
        <nav class="dark-custom">
            <div class="nav-wrapper row">
                <div class="header-title col s3 hide-on-small-and-down">
                    <span style="font-weight: bolder" class="chapter-title"><b>uHub</b></span>
                </div>
                <div class="header-title col s1 push-s5 hide-on-med-and-up center">
                    <span style="font-weight: bolder" class="chapter-title"><b>uHub</b></span>
                </div>
                <form action="search.php?q" method="post" class="left search col s6 hide-on-small-and-down">
                    <div class="input-field">
                        <input id="search" style="font-size: 200px;line-height: 3;" class="left-align left" dir="auto" type="search" placeholder="Search" autocomplete="off" name="search" onkeyup="showResult(this.value)">
                    </div>
                    <a class="close-search waves-effect waves-dark"><i class="material-icons">close</i></a>
                </form>
	            <div class="search-box" id="livesearch"></div>
                <ul class="right col s6 m3 nav-right-menu">
                    <li><a data-activates="slide-out" data-activates-lol="chat-sidebar"
                           class="chat-button show-on-large waves-effect waves-light">
                        <i class="material-icons" id="slide-out-icon">menu</i></a>
                    </li>
                    <li class="hide-on-med-and-up">
                        <a class="search-toggle waves-effect waves-light">
                            <i class="material-icons">search</i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside id="slide-out" class="side-nav {opts.items.theme.class_2}">
        <div class="side-nav-wrapper">
            <div if="{user}" class="sidebar-profile">
                <div class="sidebar-profile-image right-align">
                    <img src="{user.avatar? user.avatar : '/static/assets/images/profile-image-2.png'}" class="circle"
                         alt="">
                </div>
                <div class="sidebar-profile-info">
                    <a href="javascript:void(0);" class="account-settings-link">
                        <p style="" dir="auto" class="right right-align">{user.first_name} {user.last_name}</p>
                        <span class="white-text-custom">@{user.username}</span>
                    </a>
                </div>
            </div>
            <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
                <li onclick="close_sidenav()" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-60px;font-size:110%"><b class="">صفحه اصلی</b></span><i
                        class="material-icons right">home</i></a></li>
                <li onclick="close_sidenav()" if="{!user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b class="">ثبت نام</b></span><i
                        class="material-icons right">person_add</i></a></li>
                <li onclick="close_sidenav()" if="{!user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b class="">ورود</b></span><i
                        class="material-icons right">input</i></a></li>
                <li onclick="close_sidenav()" if="{user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b class="">پروفایل</b></span><i
                        class="material-icons right">account_circle</i></a></li>
                <li onclick="close_sidenav()" if="{user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b class="">تنظیمات</b></span><i
                        class="material-icons right">settings</i></a></li>
                <li onclick="close_sidenav()" if="{user}" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b style="padding-left: 10px"
                                                                     class="">گالری</b></span><i
                        class="material-icons right">collections</i></a></li>
                <li onclick="close_sidenav()" class="no-padding center {opts.items.theme.class_2}"><a
                        class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-30px;font-size:110%"><b style="padding-left: 10px"
                                                                     class="">عکس های جدید</b></span><i
                        class="material-icons right">fiber_new</i></a></li>
                <li onclick="close_sidenav()" class="no-padding center {opts.items.theme.class_2}"><a
                        onclick="Materialize.toast('! به زودی', 4000)" class="waves-effect waves-light"
                        href="javascript:void(0);"><span
                        style="margin-right:-80px;font-size:110%"><b style="padding-right: 15px" class="">مرکز آموزش</b></span><i
                        class="material-icons right">local_library</i></a></li>

            </ul>
            <div class="footer">
                <p class="copyright">Bamboo ©</p>
            </div>
        </div>
    </aside>
    <div class="slider">
        <ul class="slides">
            <?php
			$sql2="SELECT * FROM slides ORDER BY post_time DESC";
			$result2 =$conn->query($sql2);
			if($result2->num_rows > 0){
            while($row2 = $result2->fetch_assoc())
			{ ?>
            <li><a href="/uhub/<?php echo $row2['link'] ?>">
                <img src="img/<?php echo $row2['image'] ?>"> <!-- random image -->
                <div class="caption left-align">
                    <h3><?php echo $row2['caption'] ?></h3>
                    <h5 class="light grey-text text-lighten-3"><?php echo $row2['description'] ?></h5>
                </div>
            </a></li>
            <?php } } ?>
        </ul>
    </div>
    <main class="mn-inner">
        <div class="row">
            <h4 class="left">Books</h4>
            <h5 class="right"><a href="books" class="see-all">See All<i class="material-icons">chevron_right</i></a></h5>
        </div>
        <div class="row carousels">
            <?php
			$sql="SELECT * FROM posts where post_type = 'book' ORDER BY post_time DESC LIMIT 8";
			$result =$conn->query($sql);
			if($result->num_rows > 0){
            while($row = $result->fetch_assoc())
			{ ?>
            <div class="col s2 m4 l3">
            <a href="book.php?book=<?php echo $row['link'] ?>">
                <div class="card hoverable">
                    <div class="card-image">
                        <img src="img/<?php echo $row['image'] ?>" class="responsive-img" alt="<?php echo $row['title'] ?>">
                    </div>
                    <div class="card-content">
                        <span class="card-title"><?php echo $row['title'] ?></span>
                    </div>
                </div>
            </a>
            </div>
        <?php } } ?>
        </div>
        <div class="row">
            <h4 class="left">Videos</h4>
            <h5 class="right"><a href="videos" class="see-all">See All<i class="material-icons">chevron_right</i></a></h5>
        </div>
        <div class="row carousels">
            <?php
			$sql="SELECT * FROM posts where post_type = 'video' ORDER BY post_time DESC LIMIT 8";
			$result =$conn->query($sql);
			if($result->num_rows > 0){
            while($row = $result->fetch_assoc())
			{ ?>
            <div class="col s2 m4 l3">
            <a href="video.php?video=<?php echo $row['link'] ?>">
                <div class="card hoverable">
                    <div class="card-image">
                        <img src="img/<?php echo $row['image'] ?>" class="responsive-img" />
                        <div class="overlay">
                            <div class="overlay-icon"><i class="medium material-icons">play_circle_outline</i></div>
                        </div>
                    </div>
                    <div class="card-content">
                        <span class="card-title"><?php echo $row['title'] ?></span>
                    </div>
                </div>
            </a>
            </div>
        <?php } } ?>
        </div>
        <div class="row">
            <div class="row">
                <h4 class="left">Articles</h4>
                <h5 class="right"><a href="articles" class="see-all">See All<i class="material-icons">chevron_right</i></a></h5>
            </div>
            <div class="row carousels">
                <?php
                $sql="SELECT * FROM posts where post_type = 'article' ORDER BY post_time DESC LIMIT 8";
                $result =$conn->query($sql);
                if($result->num_rows > 0){
                while($row = $result->fetch_assoc())
                { ?>
                <div class="col s12 m4">
                    <div class="card hoverable">
                        <a href="#"><div class="card-content"><h4 class="card-title truncate">Literature</h4></div>
                        <div class="card-image">
                            <img src="img/<?php echo $row['image'] ?>" alt="">
                        </div>
                        <div class="card-content">
                            <span class="card-title truncate"><?php echo $row['title'] ?></span>
                            <p class="truncate"><?php echo $row['content'] ?></p>
                        </div></a>
                    </div>
                </div>
                <?php } } ?>
            </div>
        </div>
    </main>
    <footer-box></footer-box>
</div>
<!--Import Scripts-->
<script src="static/plugins/jquery/jquery-2.2.0.min.js"></script>
<script src="static/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="static/plugins/jquery-blockui/jquery.blockui.js"></script>
<script src="static/plugins/slick/slick.min.js"></script>
<script src="static/plugins/sweetalert/sweetalert.min.js"></script>
<script src="static/plugins/materialize/js/materialize.min.js"></script>
<script src="static/js/alpha.js"></script>
<script src="static/js/uhub.js"></script>
</body>
</html>