<?php
date_default_timezone_set('Asia/Tehran');
include_once 'dbconfig.php';
$id = @$_GET['id'];
$idup = @$_POST['id'];
$title = @$_POST['title'];
$author = @$_POST['author'];
//$tags = @$_POST['tags'];
$content = @$_POST['content'];
$post_type = @$_POST['post_type'];
	if(isset($_POST['update'])) {
		$total = count($_FILES['upload']['name']);
		$timestamp = date('Y-m-d G:i:s');
		$link = str_replace(' ','-',strtolower($title));
		for($i=0; $i<$total; $i++) {
			$uploaded = rand(1000,100000)."-".$_FILES['upload']['name'][$i];
			$file_loc = $_FILES['upload']['tmp_name'][$i];
			$new_file_name = strtolower($uploaded);
			$imgs="img/";
			$files="ups/";
			$final_file[] = str_replace(' ','-',$new_file_name);
			if($i==0){
				$img=$final_file[0];
				move_uploaded_file($file_loc,$imgs.$img);
			}
			else{
				$file=$final_file[1];
				move_uploaded_file($file_loc,$files.$file);
			}
		}

		$cover=$final_file[0];
		$dlfile=$final_file[1];
		$sql="UPDATE posts SET title='".$title."',author='".$author."',image='".$cover."',content='".$content."',post_type='".$post_type."',file='".$dlfile."',link='".$link."',post_time='".$timestamp."' WHERE id='".$idup."'";

			if ($conn->query($sql) === TRUE) {
				echo "Updated!";
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}
			$conn->close();
		}
	
	if(isset($_GET['id'])){
		$q="SELECT * FROM posts WHERE id='$id'";
		$result=$conn->query($q);
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>UHub - Editing <?php echo $row['title'] ?></title>
	<script src="js/jquery-3.1.1.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="/uhub/ckeditor/ckeditor.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<style>
		body{
			background-color: lightslategray;
		}
		a{
			color: white;
			transition: all 0.35s ease-out;
		}
		a:hover{
			color: black;
			text-decoration: none;
		}
		a:visited{
			text-decoration: none;
		}
		a:link{
			color: white;
			text-decoration: none;
		}
		.row{
			margin: 30px auto;
			text-align: center;
		}
		.head{
			background-color: darkorange;
			height: 50px;
			color: white;
			text-align: center;
			font-size: 35px;
		}
		form{
			border: 2px solid darkorange;
			padding: 10px;
			border-radius: 10px;
			background-color: rgba(249, 168, 69, 0.21);
			box-shadow: 3px 6px 17px 1px #777777;
		}
		h4{
			color: white;
			background-color: darkorange;
			border-radius: 15px;
			padding: 5px;
		}
		input[type=text]{
			display: inline-block;
			width: 100%;
			margin-bottom: 15px; 
		}
		input[type=file]{
			display: inline-block;
			margin: 0 0 15px 10px; 
		}
		textarea{
			width: 100%;
		}
		input[type=submit]{
			display: inline-block;
			background-color: darkorange;
			color: white;
			border: 0px;
			transition: all 0.3s linear;
			width: 100%;
			margin: 5px 0 15px 0;
			box-shadow: 3px 6px 17px 1px #777777;
 		}
		input[type=submit]:hover{
			background-color: #d47706;
			box-shadow: 0px 0px 07px 0px #777777;
		}
		input[type=radio]{
			margin: 5px;
		}
		label{
			color: white;
		}
		.footer{
			visibility: visible;
			background-color: darkorange;
			height: 50px;
			padding: 15px;
			color: white;
			bottom: 0px;
			left: 0px;
			position: fixed;
			text-align: center;
			width: 100%;
			z-index: 99;
			box-shadow: 0px 0px 07px 0px #777777;
		}
		.separator{
			padding: 0 15px;
		}
	</style>
</head>
<body onload="loadtext();">
	<div id="top" class="head"><?php echo $row['title'] ?></div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-xs-12 col-md-offset-3">
				<form action="postedit.php" method="POST" enctype="multipart/form-data" required>
					<input type="text" placeholder="Title" name="title" value="<?php echo $row['title'] ?>" required>
					<input type="text" placeholder="Author" name="author" value="<?php echo $row['author'] ?>" required>
					<!--<input type="text" placeholder="Tags" name="tags" required>-->
					<label>Cover Image:</label><input type="file" name="upload[]" id="cover" accept="image/*" multiple="multiple" required/>
					<img id="preview" src="img/<?php echo $row['image'] ?>" width="100" height="100">
					<textarea id="editor1" rows="8" placeholder="Text Description" name="content" required><?php echo $row['content'] ?></textarea>
					<script>CKEDITOR.replace( 'editor1' );</script>
					<label>Downloadable Content:</label><input type="file" name="upload[]" multiple="multiple" /><br>
					<?php if($row['post_type']=='video'){ ?>
						<label>Post Type:</label><input type="radio" name="post_type" value="video" checked>Video
						<input type="radio" name="post_type" value="book">Book
						<input type="radio" name="post_type" value="article">Article
					<?php } 
					else if($row['post_type']=='book'){ ?>
						<label>Post Type:</label><input type="radio" name="post_type" value="video">Video
						<input type="radio" name="post_type" value="book" checked>Book
						<input type="radio" name="post_type" value="article">Article
					<?php }
					else{ ?>
						<label>Post Type:</label><input type="radio" name="post_type" value="video">Video
						<input type="radio" name="post_type" value="book">Book
						<input type="radio" name="post_type" value="article" checked>Article
					<?php } ?>
					<input type="hidden" name="id" value="<?php echo $row['id'] ?>">
					<input type="submit" value="Update" name="update">
					<input type="hidden" name="MAX_FILE_SIZE" value="512000"/>
				</form>
				<?php } } }
					else{
						header("Location: post.php");
					}
				 ?>
			</div>
		</div>
	</div>
	<script src="static/js/uhub.js"></script>
</body>

</html>